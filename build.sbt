name := "1MSS"

version := "0.1"

scalaVersion := "2.13.1"

scalacOptions += "-target:8"
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

javaOptions in run += "-Xmx4G"
scalacOptions += "-deprecation"
resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-snapshot-local/"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/releases"
libraryDependencies += "oscar" %% "oscar-cp" % "4.1.0-SNAPSHOT" withSources()
mainClass in assembly := Some("oscar1mss.runners.CompleteRunner")
libraryDependencies += "junit" % "junit" % "4.12" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"
libraryDependencies += "org.scalatestplus" %% "scalatestplus-junit" % "1.0.0-M2"


libraryDependencies += "org.json" % "json" % "20180813"

libraryDependencies ~= { _ map (m => {
  if(m.organization == "oscar") {
    m.exclude("xerces", "xmlParserAPIs").exclude("jaxen", "jaxen")
  }
  else if(m.organization == "org.typelevel") {
    m.exclude("org.typelevel", "spire-platform")
  }
  else
    m
})}

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

libraryDependencies += "com.storm-enroute" %% "scalameter" % "0.19"

testFrameworks += new TestFramework("org.scalameter.ScalaMeterFramework")

parallelExecution in Test := false

scalacOptions in Compile ++= Seq("-feature", "-opt-warnings", "-opt:l:method", "-opt:l:inline", "-opt-inline-from:scala.**,oscar.**,oscar1mss.**")
