#!/usr/local/bin/python3
import math
import os, shutil, sys
from collections import deque
import random
import multiprocessing
import matplotlib
import matplotlib.pyplot as plt



def_size = [(80, 20), (64, 25), (50, 32), (40, 40)]
def_distr = [(-3.0, 1.0)]
def_sol_distr = [(1.0, 1.0)]
def_k = [2, 5]
def_cover = [0.2, 0.5]
def_thetas = [("median", 1-0.2), ("median", 1-0.3), ("median", 1-0.4), ("median", 1-0.5), ("median", 1-0.6), ("median", 1-0.7), ("median", 1-0.8)]
def_seed = [42, 43]


data_dir = sys.argv[1]
ref_dir = sys.argv[2]


force_plot = False
showplot = "-plot" in sys.argv or force_plot
if showplot:
    sys.argv = [x for x in sys.argv if not x == "-plot"]

def_clear_all = False

if len(sys.argv) < 3:
    print("path_data path_ref\n" \
        "\t[size_x_1]x[size_y_1][,[size_x_2]x[size_y_2]]\n" \
        "\t[mu_bg_1]x[sigma_bg_1][,[mu_bg_2]x[sigma_bg_2]]\n" \
        "\t[mu_sig_1]x[sigma_sig_1][,[mu_sig_2]x[sigma_sig_2]]\n" \
        "\t[k_1][,[k_2]]\n" \
        "\t[cover_1][,[cover_2]]\n" \
        "\t[theta_type_1]x[theta_value_1][,[theta_type_2]x[theta_value_2]]\n" \
        "\t[seed_1][,[seed_2]]\n" \
        "\t[-plot]\n")
    exit()

def parse_arg(arg, delimitor, separator=None, is_int=True, is_float=True):
    res = [(s[:s.index(separator)], s[s.index(separator)+1:]) if separator is not None else s for s in arg.split(delimitor)]
    if is_int:
        return [[int(x) for x in s] if separator is not None else int(s) for s in res]
    elif is_float:
        return [[float(x) for x in s] if separator is not None else float(s) for s in res]
    return res

argi = 3
sizes = parse_arg(sys.argv[argi], ",", "x", is_int=True) if len(sys.argv) > argi else def_size
argi += 1
distributions = parse_arg(sys.argv[argi], ",", "x", is_int=False) if len(sys.argv) > argi else def_distr
argi += 1
sol_distr = parse_arg(sys.argv[argi], ",", "x", is_int=False) if len(sys.argv) > argi else def_sol_distr
argi += 1
ks = parse_arg(sys.argv[argi], ",", is_int=True) if len(sys.argv) > argi else def_k
argi += 1
covers = parse_arg(sys.argv[argi], ",", is_int=False) if len(sys.argv) > argi else def_cover
argi += 1
thetas = parse_arg(sys.argv[argi], ",", "x", is_int=False, is_float=False) if len(sys.argv) > argi else def_thetas
argi += 1
seeds = parse_arg(sys.argv[argi], ",", is_int=True) if len(sys.argv) > argi else def_seed
argi += 1


# Deal with paths
if os.path.exists(data_dir) and def_clear_all:
    shutil.rmtree(data_dir)

if not os.path.exists(data_dir):
    os.makedirs(data_dir)

if os.path.exists(ref_dir) and def_clear_all:
    shutil.rmtree(ref_dir)

if not os.path.exists(ref_dir):
    os.makedirs(ref_dir)


def gaussian_matrix(nr = 800, nc = 200, mu = -1, sigma = 1):
    #mu and sigma can be either a single value or an array of the dimension
    return [[random.gauss(mu, sigma) for j in range(nc)] for i in range(nr)]

def store(path, matrix):
    with open(data_dir + path, 'w+') as f:
        for row in matrix:
            f.write('\t'.join([str(entry) for entry in row]) + '\n')


def store_solution(path, solution):
    with open(data_dir + path, 'w+') as f:
        for bicluster in solution:
            f.write('(' + ','.join([str(val) for val in bicluster[0]]) + ');')
            f.write('(' + ','.join([str(val) for val in bicluster[0]]) + ')\n')

def mean(m, loc=0.5):
    return sum([entry for row in m for entry in row])/(len(m)*len(m[0]))

def median(m, loc=0.5):
    middle = int(len(m)*len(m[0])*loc)
    return sum(sorted([entry for row in m for entry in row])[middle:middle+2])/2

#TODO: deal with this
cnt = 1
todo = []
for size in sizes:
    for distribution in distributions:
        for sol_distribution in sol_distr:
            for k in ks:
                for cover in covers:
                    for theta in thetas:
                        for seed in seeds:
                            if k != 0 or cover == covers[0]: #Prevent irrelevant combinations
                                todo.append([cnt, size, distribution, sol_distribution, k, cover if k != 0 else 0.0, theta, seed])
                                cnt += 1


def process(x):
    #TODO: deal with this
    cnt, size, distribution, sol_distribution, k, cover, theta, seed = x

    name = "{sizeX}x{sizeY}_{distrib_mu}x{distrib_sigma}_{sol_mu}x{sol_sigma}_{k}_{cover}_{theta_type}x{theta_val}_{seed}".format(
        sizeX = size[0], sizeY = size[1],
        distrib_mu = distribution[0], distrib_sigma = distribution[1],
        sol_mu = sol_distribution[0], sol_sigma = sol_distribution[1],
        k = k,
        cover = cover,
        theta_type=theta[0], theta_val=theta[1],
        seed = seed
    )

    random.seed(seed+sum(size)+sum(distribution)+sum(sol_distribution)+k+cover+float(theta[1]))  #Set the seed

    #Fill matrix with background value
    matrix = [[random.gauss(mu=distribution[0], sigma=distribution[1]) for j in range(size[1])] for i in range(size[0])]

    #Define the submatrices:
    rows = [i for i in range(size[0])]
    cols = [i for i in range(size[1])]
    size_x_submatrix = max(1, int(size[0]*math.sqrt(cover)))
    size_y_submatrix = max(1, int(size[1]*math.sqrt(cover)))
    sols = []
    for i in range(k):
        random.shuffle(rows)
        random.shuffle(cols)
        sols.append([rows[:size_x_submatrix], cols[:size_y_submatrix]])
        #Fill submatrix with values
        for x in rows[:size_x_submatrix]:
            for y in cols[:size_y_submatrix]:
                matrix[x][y] = random.gauss(mu=sol_distribution[0], sigma=sol_distribution[1])

    #Random noise to all entries
    matrix = [[entry + random.gauss(mu=0, sigma=0.1) for entry in row] for row in matrix]

    #Get the median/mean/none of all values
    threshold = 0.0
    if theta[0]=="median":
        threshold = median(matrix, float(theta[1]))
    elif theta[0]=="mean":
        threshold = mean(matrix, float(theta[1]))
    # else:
    #     print("no modif.")

    #Apply threshold
    matrix = [[entry - threshold for entry in row] for row in matrix]
    computed_mean = mean(matrix)
    computed_median = median(matrix)
    computed_rate_plus = sum([1 if ij > 0 else 0 for row in matrix for ij in row])/(size[0]*size[1])

    if showplot:
        fig, ax = plt.subplots()
        n, bins, patches = ax.hist([entry for row in matrix for entry in row], 50, density=True)
        # ax.plot(bins, y, '--')
        ax.set_xlabel('Values')
        ax.set_ylabel('Probability density')
        ax.set_title(r'Histogram of values: $\mu=' + str(distribution[0]) + r'$, $\sigma='+str(distribution[1])+r'$')

        # Tweak spacing to prevent clipping of ylabel
        fig.tight_layout()
        plt.savefig(data_dir+name+'.png')
        plt.close()
        # plt.savefig(data_dir+'matrix.png')

    #Store results
    store(name + 'matrix.tsv', matrix)
    # store('matrix.tsv', matrix) #TODO: remove
    store(name + 'solution.txt', [["Sol: {i}\n\trows: {r}\n\tcols: {c}".format(i=i, r=sol[0], c=sol[1])] for i,sol in enumerate(sols)])
    
    with open(ref_dir + str(cnt) + '.txt', 'w+') as f:
        f.write(name + '\n')
        f.write(str(1) + '\n')
        f.write("Threshold: {theta_type} {theta_val}\n".format(theta_type=theta[0], theta_val=theta[1]))
        f.write("median: " + str(computed_median) + "\nmean: " + str(computed_mean) + "\n")
        f.write("rate: " + str(computed_rate_plus) + "\n")

    # print(name)
    return True

if len(todo) == 1:
    process(todo[0])
else:
    pool = multiprocessing.Pool(8)
    ok = 0
    ko = 0
    for ret in pool.map(process, todo):
        if ret:
            ok += 1
        else:
            ko += 1

    print(ok, ko)
    #for i in map(process, todo):
    #    pass