package MaxSumSubmatrix.branchings

import MaxSumSubmatrix.constraints.utils.CurrentBoundWithHeuristicTraits
import MaxSumSubmatrix.utils.MaximizeDouble
import oscar.algo.reversible.ReversibleInt
import oscar.algo.search.Branching
import oscar.cp.core.variables.CPBoolVar

/**
  * Created by vbranders on 03/09/2020.
  *
  */
class ImmediateSolution(base: Branching,
                        rows: Array[CPBoolVar],
                        cols: Array[CPBoolVar],
                        ctrObj: MaximizeDouble,
                        bound: CurrentBoundWithHeuristicTraits,
                        epsilon: Double = 1e-8) extends Branching {

  private[this] val cp = rows(0).store
  private[this] val lastBoundRow = new ReversibleInt(cp, 0)
  private[this] val lastBoundCol = new ReversibleInt(cp, 0)

  override def alternatives(): Seq[_root_.oscar.algo.search.Alternative] = {
    //Fix all the bound variables
    while (lastBoundRow.value != rows.length && rows(lastBoundRow.value).isBound) {
      lastBoundRow.value += 1
    }
    while (lastBoundCol.value != cols.length && cols(lastBoundCol.value).isBound) {
      lastBoundCol.value += 1
    }

//    println("Branching")

    //If all are bound
    if (lastBoundRow.value == rows.length && lastBoundCol.value == cols.length) {
      Array[oscar.algo.search.Alternative]()
    }
    else if (lastBoundRow.value == rows.length) {
      Array[oscar.algo.search.Alternative](() => {
        println("All rows set!")
        bound.curBoundRows0Apply()
      })
    }
    else if (lastBoundCol.value == cols.length) {
      Array[oscar.algo.search.Alternative](() => {
        println("All cols set!")
        bound.curBoundCols0Apply()
      })
    }
    else if (bound.curBoundCols0 > bound.curBoundRows0 && certainlyLess(ctrObj.best, bound.curBoundCols0)) {
      //      newInternalSolution()
      //      println("CurBoundCols is good : " + bound.curBoundCols0 + ", " + ctrObj.best)
      Array(() => {
        bound.curBoundCols0Apply()
      }) ++ base.alternatives()
    }
    else if (bound.curBoundCols0 < bound.curBoundRows0 && certainlyLess(ctrObj.best, bound.curBoundRows0)) {
      //      newInternalSolution()
      //      println("CurBoundRows is good : " + bound.curBoundRows0 + ", " + ctrObj.best)
      Array(() => {
        bound.curBoundRows0Apply()
      }) ++ base.alternatives()
    }
    else {
      bound.checkNew()
      base.alternatives()
    }
  }

  /**
    * a < b
    */
  @inline
  def certainlyLess(a: Double, b: Double): Boolean = a - b < -epsilon
}
