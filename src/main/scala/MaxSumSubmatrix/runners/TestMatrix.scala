package MaxSumSubmatrix.runners

/**
  * Created by vbranders on 03/09/2020.
  *
  */

// LIST OF POSSIBLE TESTS
trait TestMatrix {
  def name: String

  def isSolved: Boolean

  def matrix(): Array[Array[Double]]

  def solution: Double
}

// Test from generated data
object Matrix1 extends TestMatrix {
  override def name: String = "-1"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = MaxSumSubmatrix.runners.Utils.getMatrix("data/tmp/matrix.txt")

  override def solution: Double = 278.924256374684
}

object Matrix2 extends TestMatrix {
  override def name: String = "-2"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = MaxSumSubmatrix.runners.Utils.getMatrix("data/tmp/matrix2.txt")

  override def solution: Double = 27.3
}

object Matrix3 extends TestMatrix {
  override def name: String = "-3"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = MaxSumSubmatrix.runners.Utils.getMatrix("data/tmp/matrix3.txt")

  override def solution: Double = 58.72283654167315
}

object Matrix4 extends TestMatrix {
  override def name: String = "-4"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = MaxSumSubmatrix.runners.Utils.getMatrix("data/tmp/matrix.tsv")

  override def solution: Double = 58.72283654167315
}

object Matrix5 extends TestMatrix {
  override def name: String = "-5"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = MaxSumSubmatrix.runners.Utils.getMatrix("data/eval/40x40_-3.0x1.0_1.0x1.0_0_0.0_medianx0.8_42matrix.tsv")

  override def solution: Double = 13.832878774108014
}

// Test from build data to prove wrong to certain bounds
object Test1 extends TestMatrix {
  override def name: String = "1"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(20, -2, -2, -4),
    Array(-5, 4, 2, 2)
  )

  override def solution: Double = 20.0
}


object Test2 extends TestMatrix {
  override def name: String = "2"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(20, -2, -2, -4),
    Array(-5, 2, 2, 2)
  )

  override def solution: Double = 20.0
}


object Test3 extends TestMatrix {
  override def name: String = "3"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(20, -2, -2, -4),
    Array(-5, 2, 2, 2),
    Array(0, 2, 2, 2)
  )

  override def solution: Double = 20.0
}


object Test4 extends TestMatrix {
  override def name: String = "4"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(20, -2, -2, -4),
    Array(-5, 2, 2, 2),
    Array(0, 2, 2, 2),
    Array(0, 2, 2, 2)
  )

  override def solution: Double = 25.0
}


object Test5 extends TestMatrix {
  override def name: String = "5"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(20, -1, -2, -1.1),
    Array(-3, 2, 2, 2),
    Array(0, 1.1, 0, 0)
  )

  override def solution: Double = 20.1
}


object Test6 extends TestMatrix {
  override def name: String = "6"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(10, 7),
    Array(0.5, -2),
    Array(0.5, -2)
  )

  override def solution: Double = 17
}


object Test7 extends TestMatrix {
  override def name: String = "7"

  override def isSolved: Boolean = true

  override def matrix(): Array[Array[Double]] = Array(
    Array(10, 3),
    Array(0.5, -2),
    Array(0.5, -2)
  )

  override def solution: Double = 13
}


//////////////////////////////////
