package MaxSumSubmatrix.runners

import java.lang.management.ManagementFactory

import scala.io.Source

object Utils {
  def getMatrix(filePath: String): Array[Array[Double]] = {
    val SrcFile = Source.fromFile(filePath)
    val m = SrcFile.getLines().toArray.map(_.split("\t").map(_.toDouble))
    SrcFile.close()
    if (m.length >= m(0).length)
      m
    else
      m.transpose
  }

  def getCpuTime: Long = {
    val bean = ManagementFactory.getThreadMXBean
    if (bean.isCurrentThreadCpuTimeSupported) bean.getCurrentThreadCpuTime
    else 0L
  }
}
