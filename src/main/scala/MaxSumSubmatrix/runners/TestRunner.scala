package MaxSumSubmatrix.runners

import MaxSumSubmatrix.constraints.utils._
import MaxSumSubmatrix.runners.Utils.getMatrix
import MaxSumSubmatrix.utils
import MaxSumSubmatrix.utils.MaximizeDouble
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

import scala.io.Source

/**
  * Created by vbranders on 03/09/2020.
  *
  */

object TestRunner extends App {

  // Listing possible constraint methods
  val possibleConstraintMethods: Map[String, ConstraintMethod] = Array(
    BrandersEtAl,
    BrandersNew
  ).map(x => x.name -> x).toMap

  // Listing possible branching methods
  val possibleBranchingMethods: Map[String, BranchingMethod] = Array(
    StaticBranchingMethod
  ).map(x => x.name -> x).toMap


  // List possible test matrices
  val possibleTestMatrices: Map[String, TestMatrix] = Array(
    Test1, Test2, Test3, Test4, Test5, Test6, Test7, Matrix1, Matrix2, Matrix3, Matrix4, Matrix5
  ).map(x => x.name -> x).toMap
  // To get parameters
  val (filePaths, timeBudget, branchingMethod, constraintMethod) = try {
    (args(0), args(1).toInt, possibleBranchingMethods(args(2)), possibleConstraintMethods(args(3)))
  } catch {
    case x: Exception =>
      displayHelp()
      throw x
  }

  // To display help
  if (args.length == 0 || args(0) == "-h" || args(0) == "--help") {
    displayHelp()
    System.exit(0)
  }
  else if (args.length == 1 && (args(0) == "-l" || args(0) == "--list")) {
    displayArgList()
    System.exit(0)
  }

  // To display help
  def displayHelp(): Unit = {
    println("Format: filepath timeBudgetInSeconds branchingMethod MatrixConstraint args")
    displayArgList()
  }

  // To display list of possible arguments
  def displayArgList(): Unit = {
    println("Possible values for matrix MaxSumSubmatrix.constraints:")
    println("- " + possibleConstraintMethods.keys.mkString("\n- "))
    println("Possible values for branching method:")
    println("- " + possibleBranchingMethods.keys.mkString("\n- "))
  }

  var success: Int = 0
  val filePathsSplit: Array[String] = filePaths.split("\\+")

  for (filePath <- filePathsSplit) {
    implicit val cp: CPSolver = new CPSolver()

    val isTest: Boolean = possibleTestMatrices.contains(filePath)
    val matrix: Array[Array[Double]] = if (isTest) {
      possibleTestMatrices(filePath).matrix()
    } else {
      if(filePath.startsWith(":")){
        println(filePath)
        val path_eval = "data/eval/"
        val SrcFile = Source.fromFile(path_eval+filePath.substring(1)+".txt")
        val file_name = path_eval+SrcFile.getLines().toArray.apply(0)+"matrix.tsv"
        SrcFile.close()
        println(file_name)
        getMatrix(file_name)
      } else {
        getMatrix(filePath)
      }
    }

    val rows: Array[CPBoolVar] = Array.tabulate(matrix.length)(_ => CPBoolVar())
    val cols: Array[CPBoolVar] = Array.tabulate(matrix(0).length)(_ => CPBoolVar())
    val ub: Double = matrix.map(x => x.filter(_ > 0).sum).sum

    val obj: utils.DoubleVar = new utils.DoubleVar(0, ub, epsilon = 1e-6)
    val ctrObj: MaximizeDouble = new MaximizeDouble(cp, obj, epsilon = 1e-6)

    cp.optimize(ctrObj)
    obj.updateMax(ub)

    // Apply constraint
    val constraint = constraintMethod.make(cp, matrix, rows, cols, obj, ub, ctrObj, args.drop(4))
    // Perform branching
    val branching = branchingMethod.make(cp, matrix, rows, cols, obj, ub, ctrObj, constraint)

    cp.search(branching)
    cp.onSolution({
      constraint.onSolution(rows.map(e => if (e.isTrue) {
        1
      } else 0), cols.map(e => if (e.isTrue) {
        1
      } else 0))
    })
    cp.onFailure({
//      println("Backtrack to :")
//      println("rows: " + rows.indices.filter(e => rows(e).isBound).map(e => if (rows(e).isTrue) {
//        e
//      } else "-" + e + "").mkString(", "))
//      println("cols: " + cols.indices.filter(e => cols(e).isBound).map(e => if (cols(e).isTrue) {
//        e
//      } else "-" + e + "").mkString(", "))
//      println("\n")
      constraint.checkNew()
    })

    // Starts
    println("\n-------\nStarting" + {
      if (isTest) {
        " test : " + filePath
      } else ""
    })
    constraint.started()
    println(cp.start(timeLimit = timeBudget))
    println("Number of nodes: " + cp.searchEngine.nNodes)
    if (isTest && possibleTestMatrices(filePath).isSolved) {
      val sol: Double = possibleTestMatrices(filePath).solution
      if (math.abs(constraint.bestSol - sol) > 1e-6) {
        println("Error: expected solution was " + sol + " but you found " + constraint.bestSol)
        sys.exit(1)
      } else {
        println("Expected solution found: " + sol + ".")
        success += 1
      }
    }
    println("\n\n")

  }
  println("Success: " + success + "/" + filePathsSplit.length)


}
