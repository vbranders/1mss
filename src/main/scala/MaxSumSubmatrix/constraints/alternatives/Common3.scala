package MaxSumSubmatrix.constraints.alternatives

import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.Constraint
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Created by vbranders on 14/12/2020.
  *
  */
class Common3(rows: Array[CPBoolVar],
              cols: Array[CPBoolVar],
              matrix: Array[Array[Double]],
              obj: DoubleVar,
              upBound: Array[ReversibleDouble],
              psum: Array[Array[ReversibleDouble]],
              pup: Array[Array[ReversibleDouble]],
//              plo: Array[Array[ReversibleDouble]],
//              psum_prev: Array[Array[Double]],
//              pup_prev: Array[Array[Double]],
              lastUnbound: Array[ReversibleSparseSet],
              doFilter: Boolean,
              epsilon: Double = 1e-8) extends Constraint(rows(0).store) {

  private[this] val ROWS: Int = 0
  private[this] val COLS: Int = 1
  private[this] val nrows: Int = rows.length
  private[this] val ncols: Int = cols.length
  private[this] val arr: Array[Array[CPBoolVar]] = Array(rows, cols)
  private[this] val dims: Array[Int] = Array(ROWS, COLS)

//  var nbUnbound: ReversibleInt = new ReversibleInt(rows(0).store, rows.length+cols.length)

  private[this] val pupbis: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))
  private[this] val pupe: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))
  private[this] val pups: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))

  val plobis_new_tmp: Array[Array[Double]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(0.0)) //TODO rename

  override def setup(l: CPPropagStrength): Unit = {
    set_values()

    rows(0).store.post(
      new Base2(
        rows, cols, matrix, obj, upBound, psum, pupbis, pupe, pups, lastUnbound, doFilter, epsilon, 0
      )
    )
    rows(0).store.post(
      new Base2(
        cols, rows, matrix.transpose, obj, upBound.reverse, psum.reverse, pupbis.reverse, pupe.reverse, pups.reverse, lastUnbound.reverse, doFilter, epsilon, 1
      )
    )

    for(i <- rows.indices){
      if(rows(i).isBound){
        bind()
      }
      rows(i).filterWhenBind(){
        bind()
        false
      }
    }

    for(j <- cols.indices){
      if(cols(j).isBound){
        bind()
      }
      cols(j).filterWhenBind(){
        bind()
        false
      }
    }

    for(dim <- dims){
      filter(dim)
    }
    filter_duo()
    for(dim <- dims) {
      filter2(dim)
    }
  }
  def m(dim: Int, e: Int, d2e: Int): Double = if (dim == 0) { matrix(e)(d2e) } else matrix(d2e)(e)
  def max(x: Double, y: Double = 0.0): Double = math.max(x, y)
  def min(x: Double, y: Double = 0.0): Double = math.min(x, y)


  def bind(): Unit = {
    set_values()
    filter(ROWS)
    filter(COLS)
    filter_duo()
    filter2(ROWS)
    filter2(COLS)
  }

  def filter_duo(): Unit = {
    var i = 0
    var j = 0
    while(i < nrows){
      if(lastUnbound(ROWS).hasValue(i) && psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0){
        j = 0
        while(j < ncols){
//        for(j <- cols.indices){
          if(lastUnbound(COLS).hasValue(j) && matrix(i)(j) > 0 && psum(ROWS)(i).value + pupbis(ROWS)(i).value + psum(COLS)(j).value + pupbis(COLS)(j).value - matrix(i)(j)  < 0 && psum(COLS)(j).value + pupe(COLS)(j).value >= 0){
            rows(i).assignFalse()
            cols(j).assignFalse()
          }
          j += 1
        }
      }
      i += 1
    }
  }
  def filter(dim: Int): Unit = {
    var e = 0
    var maxE = arr(dim).length
    while(e < maxE){
//    for(e <- arr(dim).indices){
      if(lastUnbound(dim).hasValue(e)){
//        if(psum(dim)(e).value + plobis_new_tmp(dim)(e) > epsilon){
//          arr(dim)(e).assignTrue()
//        }
        if(psum(dim)(e).value + pupe(dim)(e).value < 0){
          arr(dim)(e).assignFalse()
        }
      }
      e += 1
    }
  }
  def filter2(dim: Int): Unit = {
    var e = 0
    var maxE = arr(dim).length
    while(e < maxE){
//    for(e <- arr(dim).indices){
      if(lastUnbound(dim).hasValue(e)){
        if(psum(dim)(e).value + plobis_new_tmp(dim)(e) > epsilon){
          arr(dim)(e).assignTrue()
        }
      }
      e += 1
    }
  }

  def set_values(): Unit = {
    //Set partial upper bound bis
    var i = 0
    var j = 0
    while(j < ncols){
//    for(j <- cols.indices){
      if(lastUnbound(COLS).hasValue(j)) {
        pupbis(COLS)(j) := 0.0
        pupe(COLS)(j) := 0.0
        pups(COLS)(j) := 0.0
        plobis_new_tmp(COLS)(j) = 0.0
      }
      j += 1
    }
    while(i < nrows){
//    for(i <- rows.indices){
      if(lastUnbound(ROWS).hasValue(i)){
        pupbis(ROWS)(i) := 0.0
        pupe(ROWS)(i) := 0.0
        pups(ROWS)(i) := 0.0
        plobis_new_tmp(ROWS)(i) = 0.0
        if(psum(ROWS)(i).value + pup(ROWS)(i).value >= 0) {
          j = 0
          while(j < ncols){
//          for (j <- cols.indices) {
            if (lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pup(COLS)(j).value >= 0) {
              if (matrix(i)(j) > 0) {
                pupbis(ROWS)(i) += matrix(i)(j)
                pupbis(COLS)(j) += matrix(i)(j)
              }
            }
            j += 1
          }
        }
      }
      i += 1
    }
    //Set partial upper bound expected
    i = 0
    while(i < nrows){
//    for(i <- rows.indices){
      if(lastUnbound(ROWS).hasValue(i) && psum(ROWS)(i).value + pupbis(ROWS)(i).value >= 0){
        j = 0
        while(j < ncols){
//        for(j <- cols.indices){
          if(lastUnbound(COLS).hasValue(j) && matrix(i)(j) > 0 && psum(COLS)(j).value + pupbis(COLS)(j).value >= 0){
            pupe(ROWS)(i) += min(matrix(i)(j), psum(COLS)(j).value + pupbis(COLS)(j).value)
            pupe(COLS)(j) += min(matrix(i)(j), psum(ROWS)(i).value + pupbis(ROWS)(i).value)
          }
          j += 1
        }
      }
      i += 1
    }
    //Set partial minimal selection from pupe excessive computation
    //Set minimal contribution of row and column
    var entry: Double = 0.0
    i = 0
    while(i < nrows){
//    for(i <- rows.indices){
      if(lastUnbound(ROWS).hasValue(i) && psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0){
        j = 0
        while(j < ncols){
//        for(j <- cols.indices){
          if(lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pupe(COLS)(j).value >= 0){
            entry = matrix(i)(j)
            if(entry > 0){
              pups(ROWS)(i) += max(entry - min(entry, psum(ROWS)(i).value + pupbis(ROWS)(i).value), max(min(psum(COLS)(j).value) + entry))
              pups(COLS)(j) += max(entry - min(entry, psum(COLS)(j).value + pupbis(COLS)(j).value), max(min(psum(ROWS)(i).value) + entry))
            } else if(entry < 0){
              plobis_new_tmp(ROWS)(i) += max(entry, -(psum(COLS)(j).value + pupe(COLS)(j).value))
              plobis_new_tmp(COLS)(j) += max(entry, -(psum(ROWS)(i).value + pupe(ROWS)(i).value))
            }
          }
          j += 1
        }
      }
      i += 1
    }
  }

  override def associatedVars(): Iterable[CPVar] = rows ++ cols

}
