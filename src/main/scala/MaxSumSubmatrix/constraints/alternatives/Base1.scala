package MaxSumSubmatrix.constraints.alternatives

import MaxSumSubmatrix.constraints.utils.BoundConstraint
import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

object Base1 {
  def apply(
             rows: Array[CPBoolVar],
             cols: Array[CPBoolVar],
             matrix: Array[Array[Double]],
             obj: DoubleVar,
             upBound: Array[ReversibleDouble],
             psum: Array[Array[ReversibleDouble]],
             pup: Array[Array[ReversibleDouble]],
             psum_prev: Array[Array[Double]],
             pup_prev: Array[Array[Double]],
             lastUnbound: Array[ReversibleSparseSet],
             doFilter: Boolean,
             epsilon: Double = 1e-8
           ): Array[BoundConstraint] = {
    Array(
      new Base1(
        rows, cols, matrix, obj, upBound, psum, pup, psum_prev, pup_prev, lastUnbound, doFilter, epsilon, 0
      ),
      new Base1(
        cols, rows, matrix.transpose, obj, upBound.reverse, psum.reverse, pup.reverse, psum_prev.reverse, pup_prev.reverse, lastUnbound.reverse, doFilter, epsilon, 1
      )
    )
  }
}

class Base1(
                 rows: Array[CPBoolVar], cols: Array[CPBoolVar],
                 matrix: Array[Array[Double]],
                 obj: DoubleVar,
                 upBound: Array[ReversibleDouble],
                 psum: Array[Array[ReversibleDouble]],
                 pup: Array[Array[ReversibleDouble]],
                 psum_prev: Array[Array[Double]],
                 pup_prev: Array[Array[Double]],
                 lastUnbound: Array[ReversibleSparseSet],
                 doFilter: Boolean,
                 epsilon: Double = 1e-8,
                 orient: Int) extends BoundConstraint(rows(0).store) {

  val ROWS: Int = 0
  val COLS: Int = 1

  val look_ahead_in: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.tabulate(if(dim == ROWS){rows.length} else cols.length)(_ => new ReversibleDouble(rows(0).store, 0.0)))
  val look_ahead_out: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.tabulate(if(dim == ROWS){rows.length} else cols.length)(_ => new ReversibleDouble(rows(0).store, 0.0)))

  override def setup(l: CPPropagStrength): Unit = {
    for (i <- rows.indices) {
      if(rows(i).isBound){bindRow(i)}
      rows(i).filterWhenBind() {
        bindRow(i)
        false
      }
    }
    for (j <- cols.indices) {
      if(cols(j).isBound){bindCol(j)}
      cols(j).filterWhenBind() {
        bindCol(j)
        false
      }
    }

    upBound(0) := compute_bound()

    //Init the look_ahead
    for(dim <- Array(ROWS, COLS)){
      for(i <- look_ahead_in(dim).indices){
        look_ahead_in(dim)(i) := compute_look_in(dim, i)
        look_ahead_out(dim)(i) := compute_look_out(dim, i)
      }
    }

    if (doFilter) {
      for (i <- rows) {
        i.callPropagateWhenBind(this)
      }
      for (j <- cols) {
        j.callPropagateWhenBind(this)
      }
    }
    obj.callPropagateWhenBoundsChange(this)
    priorityL2 = oscar.cp.core.CPStore.MinPriorityL2 + 1
  }


  def bindRow(a: Int): Unit = {
    if(rows(a).isTrue){
      upBound(0) += look_ahead_in(ROWS)(a)
    } else {
      upBound(0) += look_ahead_out(ROWS)(a)
    }
    obj.updateMax(upBound(0))
    //Do not update look_ahead on rows
    if(rows(a).isTrue) {
      for (y <- lastUnbound(COLS)) {
        look_ahead_in(COLS)(y) += min(m(a, y)) - max(aup(ROWS)(a) + min(m(a, y))) + max(aup(ROWS)(a))
        look_ahead_out(COLS)(y) += - max(m(a, y)) - max(aup(ROWS)(a) - max(m(a, y))) + max(aup(ROWS)(a))
      }
    } else {
      for (y <- lastUnbound(COLS)) {
        look_ahead_in(COLS)(y) += - max(aup(ROWS)(a) + min(m(a, y))) + max(aup(ROWS)(a))
        look_ahead_out(COLS)(y) += - max(aup(ROWS)(a) - max(m(a, y))) + max(aup(ROWS)(a))
      }
    }
  }

  def bindCol(b: Int): Unit = {
    if(cols(b).isTrue){
      upBound(0) += look_ahead_in(COLS)(b)
    } else {
      upBound(0) += look_ahead_out(COLS)(b)
    }
    obj.updateMax(upBound(0))
    for(x <- lastUnbound(ROWS)){
      look_ahead_in(ROWS)(x) := compute_look_in(ROWS, x)
      look_ahead_out(ROWS)(x) := compute_look_out(ROWS, x)
    }
    for(y <- lastUnbound(COLS)){
      var delta_in: Double = 0.0
      var delta_out: Double = 0.0
      for(i <- lastUnbound(ROWS)){
        delta_in += max(aup(ROWS)(i) + min(m(i,y))) - max(aup(ROWS)(i))
        delta_in -= max(aupPrev(ROWS)(i) + min(m(i,y))) - max(aupPrev(ROWS)(i))
        delta_out += max(aup(ROWS)(i) - max(m(i,y))) - max(aup(ROWS)(i))
        delta_out -= max(aupPrev(ROWS)(i) - max(m(i,y))) - max(aupPrev(ROWS)(i))
      }
      if(delta_in != 0){look_ahead_in(COLS)(y) += delta_in}
      if(delta_out != 0){look_ahead_out(COLS)(y) += delta_out}
    }
  }


  override def propagate(): Unit = filter()

  @inline  def filter(): Unit = {
    for(dim <- Array(ROWS, COLS)){
      for(e <- psum(dim).indices if lastUnbound(dim).hasValue(e)){
        prune(dim, e)
      }
    }
  }
  def is_prunable_in(dim: Int, e: Int): Boolean = {upBound(0).value + look_ahead_in(dim)(e) <= obj.min - epsilon}
  def is_prunable_out(dim: Int, e: Int): Boolean = {upBound(0).value + look_ahead_out(dim)(e) <= obj.min - epsilon}

  def prune(dim: Int, e: Int): Unit = {
    if(is_prunable_in(dim, e)){
      variable(dim, e).assignFalse()
    }
    if(is_prunable_out(dim, e)){
      variable(dim, e).assignTrue()
    }
  }
  @inline
  def which(dim: Int): String = if (dim == 0) {
    "rows"
  } else "cols"

  def m(i: Int, j: Int): Double = matrix(i)(j)
  def aup(dim: Int)(e: Int): Double = psum(dim)(e).value + pup(dim)(e).value
  def aupPrev(dim: Int)(e: Int): Double = psum_prev(dim)(e) + pup_prev(dim)(e)

  def compute_bound(): Double = {
    var l: Double = 0.0
    for(i <- rows.indices){
      if(lastUnbound(ROWS).hasValue(i)){
        l += max(aup(ROWS)(i))
      }
      else if (rows(i).isTrue){
        l += aup(ROWS)(i)
      }
    }
    l
  }
  def compute_look_in(dim: Int, e: Int): Double = {
    if(dim == ROWS){
      min(aup(ROWS)(e))
    } else {
      var l: Double = 0.0
      for(i <- rows.indices){
        if(lastUnbound(ROWS).hasValue(i)){
          l += max(aup(ROWS)(i) + min(m(i,e))) - max(aup(ROWS)(i))
        }
        else if(rows(i).isTrue){
          l += min(m(i,e))
        }
      }
      l
    }
  }
  def compute_look_out(dim: Int, e: Int): Double = {
    if(dim == ROWS){
      - max(aup(ROWS)(e))
    } else {
      var l: Double = 0.0
      for(i <- rows.indices){
        if(lastUnbound(ROWS).hasValue(i)){
          l += max(aup(ROWS)(i) - max(m(i,e))) - max(aup(ROWS)(i))
        }
        else if(rows(i).isTrue){
          l += - max(m(i,e))
        }
      }
      l
    }
  }


  override def associatedVars(): Iterable[CPVar] = rows ++ cols

  def variable(dim: Int, element: Int): CPBoolVar = {
    if (dim == 0) {
      rows(element)
    } else {
      cols(element)
    }
  }
}
