package MaxSumSubmatrix.constraints.alternatives

import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.reversible.{ReversibleDouble, ReversibleInt, ReversibleSparseSet}
import oscar.cp.Constraint
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Created by vbranders on 24/11/2020.
  *
  */
class Common2(rows: Array[CPBoolVar],
              cols: Array[CPBoolVar],
              matrix: Array[Array[Double]],
              obj: DoubleVar,
              upBound: Array[ReversibleDouble],
              psum: Array[Array[ReversibleDouble]],
              pup: Array[Array[ReversibleDouble]],
              plo: Array[Array[ReversibleDouble]],
              psum_prev: Array[Array[Double]],
              pup_prev: Array[Array[Double]],
              lastUnbound: Array[ReversibleSparseSet],
              doFilter: Boolean,
              epsilon: Double = 1e-8) extends Constraint(rows(0).store) {

  val ROWS: Int = 0
  val COLS: Int = 1

  var nbUnbound: ReversibleInt = new ReversibleInt(rows(0).store, rows.length+cols.length)

  private[this] val pupbis: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))
  private[this] val pupe: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))
  private[this] val pups: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(rows(0).store, 0)))

//  val pups_new_tmp: Array[Array[Double]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(0.0)) //TODO rename
  val plobis_new_tmp: Array[Array[Double]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(0.0)) //TODO rename

  override def setup(l: CPPropagStrength): Unit = {
    for(i <- rows.indices if lastUnbound(ROWS).hasValue(i) && psum(ROWS)(i).value + pup(ROWS)(i).value >= 0){
      for(j <- cols.indices if lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pup(COLS)(j).value >= 0){
        val x = matrix(i)(j)
        if(x > 0) {
          pupbis(ROWS)(i) += x
          pupbis(COLS)(j) += x
//        } else {
        }
      }
    }
    for(i <- candidates(ROWS) if psum(ROWS)(i).value + pupbis(ROWS)(i).value >= 0){
      pupe(ROWS)(i) += candidates(COLS).filter(j => matrix(i)(j) > 0 && psum(COLS)(j).value + pupbis(COLS)(j).value >= 0).map(j => math.min(matrix(i)(j), psum(COLS)(j).value + pupbis(COLS)(j).value)).sum
    }
    for(j <- candidates(COLS) if psum(COLS)(j).value + pupbis(COLS)(j).value >= 0){
      pupe(COLS)(j) += candidates(ROWS).filter(i => matrix(i)(j) > 0 && psum(ROWS)(i).value + pupbis(ROWS)(i).value >= 0).map(i => math.min(matrix(i)(j), psum(ROWS)(i).value + pupbis(ROWS)(i).value)).sum
    }
//    for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//      if(psum(ROWS)(i).value + pupbis(ROWS)(i).value >= 0) {
//        for (j <- cols.indices if lastUnbound(COLS).hasValue(j) && matrix(i)(j) > 0 && psum(COLS)(j).value + pupbis(COLS)(j).value >= 0) {
//          pupe(ROWS)(i) += math.min(matrix(i)(j), psum(COLS)(j).value + pupbis(COLS)(j).value)
//          pupe(COLS)(j) += math.min(matrix(i)(j), psum(ROWS)(i).value + pupbis(ROWS)(i).value)
//        }
//      }
//    }
    for(i <- candidates(ROWS) if psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0){
      pups(ROWS)(i) += candidates(COLS).filter(j => matrix(i)(j) > 0 && psum(COLS)(j).value + pupe(COLS)(j).value >= 0).map(j => math.max(matrix(i)(j) - math.min(matrix(i)(j), psum(ROWS)(i).value + pupbis(ROWS)(i).value), math.max(0, math.min(0, psum(COLS)(j).value) + matrix(i)(j)))).sum
      plobis_new_tmp(ROWS)(i) = candidates(COLS).filter(j => matrix(i)(j) < 0 && psum(COLS)(j).value + pupe(COLS)(j).value >= 0).map(j => max(matrix(i)(j), - (psum(COLS)(j).value + pupe(COLS)(j).value))).sum
    }
    for(j <- candidates(COLS) if psum(COLS)(j).value + pupe(COLS)(j).value >= 0){
      pups(COLS)(j) += candidates(ROWS).filter(i => matrix(i)(j) > 0 && psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0).map(i => math.max(matrix(i)(j) - math.min(matrix(i)(j), psum(COLS)(j).value + pupbis(COLS)(j).value), math.max(0, math.min(0, psum(ROWS)(i).value) + matrix(i)(j)))).sum
      plobis_new_tmp(COLS)(j) = candidates(ROWS).filter(i => matrix(i)(j) < 0 && psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0).map(i => max(matrix(i)(j), - (psum(ROWS)(i).value + pupe(ROWS)(i).value))).sum
    }
//    for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//      if(psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0){
//        for(j <- cols.indices if lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pupe(COLS)(j).value >= 0){
//          val x = matrix(i)(j)
//          if(x > 0) {
//            pups(ROWS)(i) += math.max(x - math.min(x, psum(ROWS)(i).value + pupbis(ROWS)(i).value), math.max(0, math.min(0, psum(COLS)(j).value) + x))
//            pups(COLS)(j) += math.max(x - math.min(x, psum(COLS)(j).value + pupbis(COLS)(j).value), math.max(0, math.min(0, psum(ROWS)(i).value) + x))
//          } else {
//            plobis_new_tmp(ROWS)(i) += max(x, - (psum(COLS)(j).value + pupe(COLS)(j).value))
//            plobis_new_tmp(COLS)(j) += max(x, - (psum(ROWS)(i).value + pupe(ROWS)(i).value))
//          }
//        }
//      }
//    }

    rows(0).store.post(
      new Base2(
        rows, cols, matrix, obj, upBound, psum, pupbis, pupe, pups, lastUnbound, doFilter, epsilon, 0
      )
    )
    rows(0).store.post(
      new Base2(
        cols, rows, matrix.transpose, obj, upBound.reverse, psum.reverse, pupbis.reverse, pupe.reverse, pups.reverse, lastUnbound.reverse, doFilter, epsilon, 1
      )
    )

    for(i <- rows.indices){
      if(rows(i).isBound){
        bind(ROWS, i)
      }
      rows(i).filterWhenBind(){
        bind(ROWS, i)
        false
      }
    }

    for(j <- cols.indices){
      if(cols(j).isBound){
        bind(COLS, j)
      }
      cols(j).filterWhenBind(){
        bind(COLS, j)
        false
      }
    }
    for(i <- candidates(ROWS) if psum(ROWS)(i).value + pupbis(ROWS)(i).value < 0 || psum(ROWS)(i).value + pupe(ROWS)(i).value < 0){
//      if(psum(ROWS)(i).value + pupbis(ROWS)(i).value < 0 || psum(ROWS)(i).value + pupe(ROWS)(i).value < 0) {
        rows(i).assignFalse()
//      }
    }
    for(j <- candidates(COLS) if psum(COLS)(j).value + pupbis(COLS)(j).value < 0 || psum(COLS)(j).value + pupe(COLS)(j).value < 0){
//      if(psum(COLS)(j).value + pupbis(COLS)(j).value < 0 || psum(COLS)(j).value + pupe(COLS)(j).value < 0) {
        cols(j).assignFalse()
//      }
    }
    for (i <- candidates(ROWS); j <- candidates(COLS) if psum(ROWS)(i).value + pupbis(ROWS)(i).value + psum(COLS)(j).value + pupbis(COLS)(j).value - math.max(0, matrix(i)(j)) < 0) {
//      if (psum(ROWS)(i).value + pupbis(ROWS)(i).value + psum(COLS)(j).value + pupbis(COLS)(j).value - math.max(0, matrix(i)(j)) < 0) {
        rows(i).assignFalse()
        cols(j).assignFalse()
//      }
    }
    for(i <- candidates(ROWS)){
      if(psum(ROWS)(i).value + plobis_new_tmp(ROWS)(i) > epsilon){
        rows(i).assignTrue()
      }
//      plobis_new_tmp(ROWS)(i) = 0.0
    }
    for(j <- candidates(COLS)){
      if(psum(COLS)(j).value + plobis_new_tmp(COLS)(j) > epsilon) {
        cols(j).assignTrue()
      }
//      plobis_new_tmp(COLS)(j) = 0.0
    }
  }
  def m(dim: Int, e: Int, d2e: Int): Double = if (dim == 0) { matrix(e)(d2e) } else matrix(d2e)(e)
  def max(x: Double, y: Double = 0.0): Double = math.max(x, y)
  def min(x: Double, y: Double = 0.0): Double = math.min(x, y)


  def bind(d: Int, x: Int): Unit = {
    nbUnbound -= 1
//    if(lastUnbound(ROWS).size+lastUnbound(COLS).size-nbUnbound.value == 0) {
    //Do not make copies
    //Store refs
    val d2: Int = (d + 1) % 2
    val dvar: Array[CPBoolVar] = if (d == 0) {
      rows
    } else cols
    val d2var: Array[CPBoolVar] = if (d == 0) {
      cols
    } else rows

    //Updates pupbis from pup on the bound dim
    val deltad: Array[Double] = dvar.indices.map(e => {if(!lastUnbound(d).hasValue(e)){0.0} else {candidates(d2).filter(e2 => m(d, e, e2) > 0 && psum(d2)(e2).value + pup(d2)(e2).value < 0 && psum_prev(d2)(e2) + pup_prev(d2)(e2) >= 0 ).map(e2 => -m(d, e, e2)).sum}}).toArray//Array.tabulate(dvar.length)
    for(e <- candidates(d) if deltad(e) != 0.0){
      pupbis(d)(e) += deltad(e)
    }
//    val deltad: Array[Double] = Array.ofDim(dvar.length)
//    for (e2 <- d2var.indices if lastUnbound(d2).hasValue(e2)) {
//    if (psum_prev(d2)(e2) + pup_prev(d2)(e2) >= 0 && psum(d2)(e2).value + pup(d2)(e2).value < 0) {
//      //updates pupbis
//      for (e <- dvar.indices if lastUnbound(d).hasValue(e) && m(d, e, e2) > 0) {
//        deltad(e) -= m(d, e, e2)
//        pupbis(d)(e) -= m(d, e, e2)
//      }
//    }
//  }

    for (e2 <- candidates(d2)) {
      //remove bound variable impact on pupe of the other dim
      pupe(d2)(e2) -= max(min(m(d, x, e2), psum_prev(d)(x) + pupbis(d)(x).value))
      pupe(d2)(e2) += candidates(d).filter(e => m(d, e, e2) > 0 && m(d, e, e2) > psum(d)(e).value + pupbis(d)(e).value).map(e => max(psum(d)(e).value + pupbis(d)(e).value) - max(min(m(d, e, e2), psum_prev(d)(e) + pupbis(d)(e).value - deltad(e)))).sum
      //      if(psum_prev(d2)(e2) + pup_prev(d2)(e2) >= 0 && psum(d2)(e2).value + pup(d2)(e2).value < 0) { //TODO: why ?
      //updates pupbis
//      for (e <- dvar.indices if lastUnbound(d).hasValue(e) && m(d, e, e2) > 0 && m(d, e, e2) > psum(d)(e).value + pupbis(d)(e).value) {
        //if pupbis has changed, potentially change pupe of the orther dim
        //Observe that psum of bound dim has not changed
        // #if new psum+pubis is larger than entry, previous psum+pubis also and min(entry, psum+pubis) is unchanged
//        pupe(d2)(e2) += max(psum(d)(e).value + pupbis(d)(e).value) - max(min(m(d, e, e2), psum_prev(d)(e) + pupbis(d)(e).value - deltad(e)))
//      }
    }

    //Updates pupbis from bound variable on the opposite dim
    val delta: Array[Double] = d2var.indices.map(e2 => {if(!lastUnbound(d2).hasValue(e2) || psum_prev(d)(x) + pup_prev(d)(x) < 0 || m(d, x, e2) <= 0){0.0} else {-m(d, x, e2)}}).toArray//Array.ofDim(d2var.length)
    if (psum_prev(d)(x) + pup_prev(d)(x) >= 0) {
      for(e2 <- candidates(d2) if delta(e2) != 0){
        pupbis(d2)(e2) += delta(e2)
      }
//    val delta: Array[Double] = Array.ofDim(d2var.length)
//      for (e2 <- d2var.indices if lastUnbound(d2).hasValue(e2) && m(d, x, e2) > 0) {
//        delta(e2) = -m(d, x, e2)
//        pupbis(d2)(e2) -= m(d, x, e2)
//      }
    }
    for(e <- candidates(d)){
      pupe(d)(e) += candidates(d2).filter(e2 => m(d, e, e2) > 0 && m(d, e, e2) > psum(d2)(e2).value + pupbis(d2)(e2).value).map(e2 => max(psum(d2)(e2).value + pupbis(d2)(e2).value) - max(min(m(d, e, e2), psum_prev(d2)(e2) + pupbis(d2)(e2).value - delta(e2)))).sum
    }
//    for (e <- dvar.indices if lastUnbound(d).hasValue(e)) {
//      //                                                          #neg entry does not impact #if new psum+pubis is larger than entry, previous psum+pubis also and min(entry, psum+pubis) is unchanged
//      for (e2 <- d2var.indices if lastUnbound(d2).hasValue(e2) && m(d, e, e2) > 0 && m(d, e, e2) > psum(d2)(e2).value + pupbis(d2)(e2).value) {
//        pupe(d)(e) += max(psum(d2)(e2).value + pupbis(d2)(e2).value) - max(min(m(d, e, e2), psum_prev(d2)(e2) + pupbis(d2)(e2).value - delta(e2)))
//      }
//    }
    for(e <- candidates(d)){
      if(psum(d)(e).value + pupe(d)(e).value >= 0) {
        pups(d)(e) := candidates(d2).filter(e2 => m(d, e, e2) > 0 && psum(d2)(e2).value + pupe(d2)(e2).value >= 0).map(e2 => max(m(d, e, e2) - max(min(m(d, e, e2), psum(d)(e).value + pupbis(d)(e).value)), max(min(psum(d2)(e2).value) + m(d, e, e2)))).sum
        plobis_new_tmp(d)(e) = candidates(d2).filter(e2 => m(d, e, e2) < 0 && psum(d2)(e2).value + pupe(d2)(e2).value >= 0).map(e2 => max(m(d, e, e2), -(psum(d2)(e2).value + pupe(d2)(e2).value))).sum
      } else {
        pups(d)(e) := 0.0
        plobis_new_tmp(d)(e) = 0.0
      }
    }
    for(e2 <- candidates(d2)){
      if(psum(d2)(e2).value + pupe(d2)(e2).value >= 0) {
        pups(d2)(e2) := candidates(d).filter(e => m(d, e, e2) > 0 && psum(d)(e).value + pupe(d)(e).value >= 0).map(e => max(m(d, e, e2) - max(min(m(d, e, e2), psum(d2)(e2).value + pupbis(d2)(e2).value)), max(min(psum(d)(e).value) + m(d, e, e2)))).sum
        plobis_new_tmp(d2)(e2) = candidates(d).filter(e => m(d, e, e2) < 0 && psum(d)(e).value + pupe(d)(e).value >= 0).map(e => max(m(d, e, e2), -(psum(d)(e).value + pupe(d)(e).value))).sum
      } else {
        pups(d2)(e2) := 0.0
        plobis_new_tmp(d2)(e2) = 0.0
      }
    }
//    for (e <- dvar.indices if lastUnbound(d).hasValue(e) && psum(d)(e).value + pupe(d)(e).value >= 0) {
//      for (e2 <- d2var.indices if lastUnbound(d2).hasValue(e2) && psum(d2)(e2).value + pupe(d2)(e2).value >= 0) {
//        val entry = m(d, e, e2)
//        if (entry > 0) {
//          pups_new_tmp(d)(e) += max(entry - max(min(entry, psum(d)(e).value + pupbis(d)(e).value)), max(min(psum(d2)(e2).value) + entry))
//          pups_new_tmp(d2)(e2) += max(entry - max(min(entry, psum(d2)(e2).value + pupbis(d2)(e2).value)), max(min(psum(d)(e).value) + entry))
//        } else {
//          plobis_new_tmp(d)(e) += max(entry, -(psum(d2)(e2).value + pupe(d2)(e2).value))
//          plobis_new_tmp(d2)(e2) += max(entry, -(psum(d)(e).value + pupe(d)(e).value))
//        }
//      }
//    }
//    for (e <- dvar.indices if lastUnbound(d).hasValue(e)) {
//      pups(d)(e).setValue(pups_new_tmp(d)(e))
//      pups_new_tmp(d)(e) = 0.0
//    }
//    for (e2 <- d2var.indices if lastUnbound(d2).hasValue(e2)) {
//      pups(d2)(e2).setValue(pups_new_tmp(d2)(e2))
//      pups_new_tmp(d2)(e2) = 0.0
//    }
    for (e <- candidates(d) if psum(d)(e).value + pupe(d)(e).value < 0) {
//      if (psum(d)(e).value + pupe(d)(e).value < 0) {
        dvar(e).assignFalse()
//      }
    }
    for (e2 <- candidates(d2)) {
      if (psum(d2)(e2).value + pupe(d2)(e2).value < 0) {
        d2var(e2).assignFalse()
      } else {
        for (e <- candidates(d) if !d2var(e2).isBound && !dvar(e).isBound && m(d, e, e2) > 0 && psum(d)(e).value + pupe(d)(e).value >= 0 && psum(d)(e).value + pupbis(d)(e).value + psum(d2)(e2).value + pupbis(d2)(e2).value - m(d, e, e2) < 0) {
          dvar(e).assignFalse()
          d2var(e2).assignFalse()
        }
      }
    }
    for (e <- candidates(d) if psum(d)(e).value + plobis_new_tmp(d)(e) > epsilon) {
//      if (psum(d)(e).value + plobis_new_tmp(d)(e) > epsilon) {
        dvar(e).assignTrue()
//      }
//      plobis_new_tmp(d)(e) = 0.0
    }
    for (e2 <- candidates(d2) if psum(d2)(e2).value + plo(d2)(e2).value > epsilon || psum(d2)(e2).value + plobis_new_tmp(d2)(e2) > epsilon) {
//      if (psum(d2)(e2).value + plo(d2)(e2).value > epsilon || psum(d2)(e2).value + plobis_new_tmp(d2)(e2) > epsilon) {
        d2var(e2).assignTrue()
//      }
//      plobis_new_tmp(d2)(e2) = 0.0
    }
//    } else {
//      println("no------------------------------------------------------------------------")
//      nbUnbound := lastUnbound(ROWS).size+lastUnbound(COLS).size
//      for(dim <- Array(ROWS, COLS)){
//        for(e <- {if(dim==ROWS){rows}else cols}.indices){
//          pupbis(dim)(e) := 0
//          pupe(dim)(e) := 0
//          pups(dim)(e) := 0
//          plobis_new_tmp(dim)(e) = 0.0
//        }
//      }
//      for(i <- rows.indices if lastUnbound(ROWS).hasValue(i) && psum(ROWS)(i).value + pup(ROWS)(i).value >= 0){
//        for(j <- cols.indices if lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pup(COLS)(j).value >= 0){
//          val x = matrix(i)(j)
//          if(x > 0) {
//            pupbis(ROWS)(i) += x
//            pupbis(COLS)(j) += x
//          }
//        }
//      }
//      for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//        if(psum(ROWS)(i).value + pupbis(ROWS)(i).value >= 0) {
//          for (j <- cols.indices if lastUnbound(COLS).hasValue(j) && matrix(i)(j) > 0 && psum(COLS)(j).value + pupbis(COLS)(j).value >= 0) {
//            pupe(ROWS)(i) += math.min(matrix(i)(j), psum(COLS)(j).value + pupbis(COLS)(j).value)
//            pupe(COLS)(j) += math.min(matrix(i)(j), psum(ROWS)(i).value + pupbis(ROWS)(i).value)
//          }
//        }
//      }
//      for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//        if(psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0){
//          for(j <- cols.indices if lastUnbound(COLS).hasValue(j) && psum(COLS)(j).value + pupe(COLS)(j).value >= 0){
//            val x = matrix(i)(j)
//            if(x > 0) {
//              pups(ROWS)(i) += math.max(x - math.min(x, psum(ROWS)(i).value + pupbis(ROWS)(i).value), math.max(0, math.min(0, psum(COLS)(j).value) + x))
//              pups(COLS)(j) += math.max(x - math.min(x, psum(COLS)(j).value + pupbis(COLS)(j).value), math.max(0, math.min(0, psum(ROWS)(i).value) + x))
//            } else {
//              plobis_new_tmp(ROWS)(i) += max(x, - (psum(COLS)(j).value + pupe(COLS)(j).value))
//              plobis_new_tmp(COLS)(j) += max(x, - (psum(ROWS)(i).value + pupe(ROWS)(i).value))
//            }
//          }
//        }
//      }
//      for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//        if(psum(ROWS)(i).value + pupbis(ROWS)(i).value < 0 || psum(ROWS)(i).value + pupe(ROWS)(i).value < 0) {
//          rows(i).assignFalse()
//        }
//      }
//      for(j <- cols.indices if lastUnbound(COLS).hasValue(j)){
//        if(psum(COLS)(j).value + pupbis(COLS)(j).value < 0 || psum(COLS)(j).value + pupe(COLS)(j).value < 0) {
//          cols(j).assignFalse()
//        }
//      }
//      for (i <- rows.indices if lastUnbound(ROWS).hasValue(i); j <- cols.indices if lastUnbound(COLS).hasValue(j)) {
//        if (psum(ROWS)(i).value + pupbis(ROWS)(i).value + psum(COLS)(j).value + pupbis(COLS)(j).value - math.max(0, matrix(i)(j)) < 0) {
//          rows(i).assignFalse()
//          cols(j).assignFalse()
//        }
//      }
//      for(i <- rows.indices if lastUnbound(ROWS).hasValue(i)){
//        if(psum(ROWS)(i).value + plobis_new_tmp(ROWS)(i) > epsilon){
//          rows(i).assignTrue()
//        }
//        plobis_new_tmp(ROWS)(i) = 0.0
//      }
//      for(j <- cols.indices if lastUnbound(COLS).hasValue(j)){
//        if(psum(COLS)(j).value + plobis_new_tmp(COLS)(j) > epsilon) {
//          cols(j).assignTrue()
//        }
//        plobis_new_tmp(COLS)(j) = 0.0
//      }
//    }
  }
  def candidates(dim: Int): Array[Int] = {
    lastUnbound(dim).toArray //TODO: do not leave .toArray and when lastUnbound is modified, create the array copy
  }
  override def associatedVars(): Iterable[CPVar] = rows ++ cols
}
