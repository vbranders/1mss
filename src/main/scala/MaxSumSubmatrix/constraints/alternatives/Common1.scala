package MaxSumSubmatrix.constraints.alternatives

import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.Constraint
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Created by vbranders on 24/11/2020.
  *
  */
class Common1(rows: Array[CPBoolVar],
              cols: Array[CPBoolVar],
              matrix: Array[Array[Double]],
              obj: DoubleVar,
              upBound: Array[ReversibleDouble],
              psum: Array[Array[ReversibleDouble]],
              pup: Array[Array[ReversibleDouble]],
              psum_prev: Array[Array[Double]],
              pup_prev: Array[Array[Double]],
              lastUnbound: Array[ReversibleSparseSet],
              doFilter: Boolean,
              epsilon: Double = 1e-8) extends Constraint(rows(0).store) {

  override def setup(l: CPPropagStrength): Unit = {
    for(i <- rows.indices){
      if(rows(i).isBound){
        bindRow(i)
      }
      rows(i).filterWhenBind(){
        bindRow(i)
        false
      }
    }
    for(j <- cols.indices){
      if(cols(j).isBound){
        bindCol(j)
      }
      cols(j).filterWhenBind(){
        bindCol(j)
        false
      }
    }
    rows(0).store.post(
      new Base1(
        rows, cols, matrix, obj, upBound, psum, pup, psum_prev, pup_prev, lastUnbound, doFilter, epsilon, 0
      )
    )
    rows(0).store.post(
      new Base1(
        cols, rows, matrix.transpose, obj, upBound.reverse, psum.reverse, pup.reverse, psum_prev.reverse, pup_prev.reverse, lastUnbound.reverse, doFilter, epsilon, 1
      )
    )
  }

  def bindRow(a: Int): Unit = {

  }
  def bindCol(b: Int): Unit = {

  }
  override def associatedVars(): Iterable[CPVar] = rows ++ cols
}
