package MaxSumSubmatrix.constraints.alternatives

import MaxSumSubmatrix.constraints.utils.BoundConstraint
import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.reversible.{ReversibleDouble, ReversibleInt, ReversibleSparseSet}
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}


//object Base2 {
//  def apply(
//             rows: Array[CPBoolVar],
//             cols: Array[CPBoolVar],
//             matrix: Array[Array[Double]],
//             obj: DoubleVar,
//             upBound: Array[ReversibleDouble],
//             psum: Array[Array[ReversibleDouble]],
//             pup: Array[Array[ReversibleDouble]],
//             psum_prev: Array[Array[Double]],
//             pup_prev: Array[Array[Double]],
//             lastUnbound: Array[ReversibleSparseSet],
//             doFilter: Boolean,
//             epsilon: Double = 1e-8
//           ): Array[BoundConstraint] = {
//    Array(
//      new Base2(
//        rows, cols, matrix, obj, upBound, psum, pup, psum_prev, pup_prev, lastUnbound, doFilter, epsilon, 0
//      ),
//      new Base2(
//        cols, rows, matrix.transpose, obj, upBound.reverse, psum.reverse, pup.reverse, psum_prev.reverse, pup_prev.reverse, lastUnbound.reverse, doFilter, epsilon, 1
//      )
//    )
//  }
//}

class Base2(
                 rows: Array[CPBoolVar], cols: Array[CPBoolVar],
                 matrix: Array[Array[Double]],
                 obj: DoubleVar,
                 upBound: Array[ReversibleDouble],
                 psum: Array[Array[ReversibleDouble]],
//                 pup: Array[Array[ReversibleDouble]],
                 pupbis: Array[Array[ReversibleDouble]],
                 pupe: Array[Array[ReversibleDouble]],
                 pups: Array[Array[ReversibleDouble]],
                 lastUnbound: Array[ReversibleSparseSet],
                 doFilter: Boolean,
                 epsilon: Double = 1e-8,
                 orient: Int) extends BoundConstraint(rows(0).store) {

  private[this] val ROWS: Int = 0
  private[this] val COLS: Int = 1
  private[this] val nrows: Int = rows.length
  private[this] val ncols: Int = cols.length

//  var nbUnbound: ReversibleInt = new ReversibleInt(rows(0).store, rows.length+cols.length)

  private[this] val check_validity: Boolean = 0==1

  override def setup(l: CPPropagStrength): Unit = {
    for (i <- rows.indices) {
      if(rows(i).isBound){bind(ROWS, i)}
      rows(i).filterWhenBind() {
        bind(ROWS, i)
        false
      }
    }
    for (j <- cols.indices) {
      if(cols(j).isBound){bind(COLS, j)}
      cols(j).filterWhenBind() {
        bind(COLS, j)
        false
      }
    }

    upBound(0) := compute_bound()

    if (doFilter) {
      for (i <- rows) {
        i.callPropagateWhenBind(this)
      }
      for (j <- cols) {
        j.callPropagateWhenBind(this)
      }
    }
    obj.callPropagateWhenBoundsChange(this)
    priorityL2 = oscar.cp.core.CPStore.MinPriorityL2 + 1
  }

  def bind(dim: Int, e: Int): Unit = {
//    nbUnbound -= 1
//    check(dim, e)
//    if(ready()){
    upBound(0).setValue(compute_bound())
    obj.updateMax(upBound(0))
//    }
  }

  override def propagate(): Unit = {
//    if(ready()){
    //Filtering on columns
    var i = 0
    var j = 0
    var look_in = 0.0
    var look_out = 0.0
    var e2 = 0
    while(j < ncols){
      if(lastUnbound(COLS).hasValue(j)) {
        look_in = 0.0
        look_out = 0.0
        if (psum(COLS)(j).value + pups(COLS)(j).value < 0) {
          look_in += psum(COLS)(j).value
        } else {
          look_out += - psum(COLS)(j).value
        }
        i = 0
        while (i < nrows) {
          if(lastUnbound(ROWS).hasValue(i)) {
            if (matrix(i)(j) < 0 && psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0) {
              if (psum(ROWS)(i).value + pupe(ROWS)(i).value + matrix(i)(j) >= 0) {
                look_in += matrix(i)(j)
              } else {
                look_in += -(psum(ROWS)(i).value + pupbis(ROWS)(i).value)
                e2 = 0
                while (e2 < ncols) {
                  if (lastUnbound(COLS).hasValue(e2) && matrix(i)(e2) > 0 && j != e2 && psum(COLS)(e2).value + pups(COLS)(e2).value >= 0) {
                    look_in += max(min(psum(ROWS)(i).value) + matrix(i)(e2))
                  }
                  e2 += 1
                }
              }
            } else if(matrix(i)(j) > 0){
              look_out += max(- matrix(i)(j), -(psum(ROWS)(i).value+pupe(ROWS)(i).value))
            }
          }
          i += 1
        }
        if (upBound(0).value + look_in <= obj.min - epsilon) {
          cols(j).assignFalse()
        }
        if (upBound(0).value + look_out <= obj.min - epsilon) {
          cols(j).assignTrue()
        }
      }
      j += 1
    }
    //Filtering on rows
    var pups_j: Double = 0.0
    var pups_j_in: Double = 0.0
    var pups_j_out: Double = 0.0
    var pupbis_j: Double = 0.0
    var delta: Double = 0.0
    i = 0
    while(i < nrows){
      if(lastUnbound(ROWS).hasValue(i)) {
        look_in = -(obj.min - upBound(0).value - epsilon)
        look_out = -(obj.min - upBound(0).value - epsilon)
        if (psum(ROWS)(i).value + pupe(ROWS)(i).value >= 0) {
          look_out -= psum(ROWS)(i).value
          j = 0
          while (j < ncols && (look_in > 0 || look_out > 0)) {
            if(lastUnbound(COLS).hasValue(j)) {
              pupbis_j = psum(COLS)(j).value + pupbis(COLS)(j).value
              pups_j = psum(COLS)(j).value + pups(COLS)(j).value
              if (matrix(i)(j) > 0) {
                pups_j_out = pups_j - max(max(
                  matrix(i)(j) - max(min(matrix(i)(j), pupbis_j)),
                  min(psum(ROWS)(i).value) + matrix(i)(j)
                ))
              } else {
                pups_j_out = pups_j
              }
              pups_j_in = pups_j_out + matrix(i)(j)
              if (pups_j >= 0) { //It was included in the bound
                if (pups_j_out < 0) { //It is no longer in case i is excluded
                  look_out -= psum(COLS)(j).value
                }
                if (pups_j_in < 0) { //It is no longer in case i is included
                  look_in -= psum(COLS)(j).value
                } else { //It is still included in case i is included : add m_i,j
                  look_in += matrix(i)(j)
                }
              } else if (pups_j_in >= 0) { //Column j was not included in the bound but when i is included, j will be also
                look_in += psum(COLS)(j).value + matrix(i)(j)
                //Given that pups_j_out can only be smaller than pups_j, combination pups_j < 0 && pups_j_out >= 0 must not be checked
              }
              if (pupbis_j >= 0) {
                if(pupbis_j - math.abs(matrix(i)(j)) < 0) {
                  delta = 0.0
                  e2 = 0
                  while (e2 < nrows) {
                    if (i != e2 && matrix(e2)(j) > 0 && lastUnbound(ROWS).hasValue(e2) && psum(ROWS)(e2).value + pupe(ROWS)(e2).value >= 0) {
                      delta += max(
                        matrix(e2)(j) - max(min(matrix(e2)(j), pupbis_j))
                      )
                    }
                    e2 += 1
                  }
                }
                if (matrix(i)(j) > 0) {
                  look_in -= matrix(i)(j)
                  look_out -= matrix(i)(j)
                  if (pupbis_j - matrix(i)(j) < 0) {
                    look_out -= delta
                    if (pups_j_out >= 0) {
                      look_out -= psum(COLS)(j).value
                    }
                  }
                } else if(pupbis_j + matrix(i)(j) < 0){
                  look_in -= delta
                  if(pups_j_in >= 0){
                    look_in -= psum(COLS)(j).value
                  }
                }
              }
            }
            j += 1
          }
        } else {
          look_in += psum(ROWS)(i).value
        }
        if (look_in <= 0) {
          rows(i).assignFalse()
        }
        if (look_out <= 0) {
          rows(i).assignTrue()
        }
      }
      i += 1
    }
//    }
  }//filter()

  @inline
  def which(dim: Int): String = if ((dim+orient)%2 == 0) {
    "rows"
  } else "cols"

  def m(i: Int, j: Int): Double = matrix(i)(j)
  def entry(dim: Int, e: Int, d2e: Int): Double = if (dim == 0) { m(e, d2e) } else m(d2e, e)

  def compute_bound(): Double = {
    var l: Double = 0.0
    l = candidates(ROWS).filter(i => psum(ROWS)(i)+pupe(ROWS)(i) >= 0).map(i => psum(ROWS)(i).value + candidates(COLS).filter(j => m(i,j) > 0 && psum(COLS)(j) + pupbis(COLS)(j) >= 0).map(j => m(i,j)).sum).sum
    l += selected(ROWS).map(i => psum(ROWS)(i).value).sum
//    for(i <- rows.indices){
//      if(lastUnbound(ROWS).hasValue(i)){
//        if(psum(ROWS)(i) + pupe(ROWS)(i) >= 0) {
//          l += psum(ROWS)(i).value
//          for (j <- cols.indices if m(i, j) > 0 && lastUnbound(COLS).hasValue(j)) {
//            if (psum(COLS)(j) + pupbis(COLS)(j) >= 0) {
//              l += m(i, j)
//            }
//          }
//        }
//      } else if(rows(i).isTrue) {
//        l += psum(ROWS)(i).value
//      }
//    }
    //TODO: used pupe rather than pupbis in paper results
    l += candidates(COLS).filter(j => psum(COLS)(j) + pups(COLS)(j) >= 0 && psum(COLS)(j) + pupbis(COLS)(j) >= 0).map(j => psum(COLS)(j).value).sum
//    for(j <- cols.indices if lastUnbound(COLS).hasValue(j) && psum(COLS)(j) + pupe(COLS)(j) >= 0 && psum(COLS)(j) + pups(COLS)(j) >= 0){
//      l += psum(COLS)(j).value
//    }
    l
  }


  override def associatedVars(): Iterable[CPVar] = rows ++ cols

  def variable(dim: Int, element: Int): CPBoolVar = {
    if (dim == 0) {
      rows(element)
    } else {
      cols(element)
    }
  }

//  def possible(dim: Int): Array[Int] = {
//    {for(e <- if(dim==0){rows.indices}else cols.indices if lastUnbound(dim).hasValue(e) || variable(dim, e).isTrue) yield e}.toArray
//  }

  def candidates(dim: Int): Array[Int] = {
    lastUnbound(dim).toArray //TODO: do not leave .toArray and when lastUnbound is modified, create the array copy
  }

  def selected(dim: Int): Array[Int] = {
    for(e <- lastUnbound(dim).removedToArray if variable(dim, e).isTrue) yield e
  }


//  @inline
//  def ready(): Boolean = lastUnbound(ROWS).size+lastUnbound(COLS).size-nbUnbound.value == 0

//  def check(d: Int, b: Int): Unit = {
//    if(check_validity) {
//      for (dim <- Array(0, 1)) {
//        for (e <- candidates(dim) if lastUnbound(dim).hasValue(e) && psum(dim)(e).value + pup(dim)(e).value >= 0) {
//          if (math.abs(pupbis(dim)(e).value - candidates(d2(dim)).filter(j => entry(dim, e, j) > 0 && psum(d2(dim))(j) + pup(d2(dim))(j) >= 0).map(j => entry(dim, e, j)).sum) > epsilon
//            || (math.abs(pupe(dim)(e).value - candidates(d2(dim)).map(j => max(min(entry(dim, e, j), psum(d2(dim))(j) + pupbis(d2(dim))(j)))).sum) > epsilon && psum(dim)(e).value + candidates(d2(dim)).map(j => max(min(entry(dim, e, j), psum(d2(dim))(j) + pupbis(d2(dim))(j)))).sum >= 0)
//            || math.abs(pups(dim)(e).value - {
//            if (psum(dim)(e) + pupe(dim)(e) < 0) {
//              0.0
//            } else {
//              candidates(d2(dim)).filter(j => psum(d2(dim))(j) + pupe(d2(dim))(j) >= 0).map(j => max(max(entry(dim, e, j)) - max(min(entry(dim, e, j), psum(dim)(e) + pupbis(dim)(e))), max(min(psum(d2(dim))(j)) + entry(dim, e, j)))).sum
//            }
//          }) > epsilon) {
//            println("Err "+(d+1)+" " + b)
//            println("rows: " + rows.indices.map(i => {
//              if (lastUnbound(ROWS).hasValue(i)) {
//                "?"
//              } else if (rows(i).isFalse) {
//                "-"
//              } else {
//                " "
//              }
//            } + i).mkString(", "))
//            println("cols: " + cols.indices.map(j => {
//              if (lastUnbound(COLS).hasValue(j)) {
//                "?"
//              } else if (cols(j).isFalse) {
//                "-"
//              } else {
//                " "
//              }
//            } + j).mkString(", "))
//            println(dim + " ! " + orient)
//            println(e)
//            println("pups: " + (psum(dim)(e).value + pup(dim)(e).value))
//            println("\npupbis: " + pupbis(dim)(e).value)
//            println("should be: " + candidates(d2(dim)).filter(j => entry(dim, e, j) > 0 && psum(d2(dim))(j) + pup(d2(dim))(j) >= 0).map(j => entry(dim, e, j)).sum)
//            println("pupbis delta: " + math.abs(pupbis(dim)(e).value - candidates(d2(dim)).filter(j => entry(dim, e, j) > 0 && psum(d2(dim))(j) + pup(d2(dim))(j) >= 0).map(j => entry(dim, e, j)).sum))
//
//            println("\npupe: " + pupe(dim)(e).value)
//            println("should be: " + candidates(d2(dim)).map(j => max(min(entry(dim, e, j), psum(d2(dim))(j) + pupbis(d2(dim))(j)))).sum)
//            println("pupe delta: " + math.abs(pupe(dim)(e).value - candidates(d2(dim)).map(j => max(min(entry(dim, e, j), psum(d2(dim))(j) + pupbis(d2(dim))(j)))).sum))
//            println("\npups: " + math.abs(pups(dim)(e).value - {
//              if (psum(dim)(e) + pupe(dim)(e) < 0) {
//                0.0
//              } else {
//                candidates(d2(dim)).filter(j => psum(d2(dim))(j) + pupe(d2(dim))(j) >= 0).map(j => max(max(entry(dim, e, j)) - max(min(entry(dim, e, j), psum(dim)(e) + pupbis(dim)(e))), max(min(psum(d2(dim))(j)) + entry(dim, e, j)))).sum
//              }
//            }))
//            println(pups(dim)(e).value)
//            println(candidates(d2(dim)).filter(j => psum(d2(dim))(j) + pupe(d2(dim))(j) >= 0).map(j => max(max(entry(dim, e, j)) - max(min(entry(dim, e, j), psum(dim)(e) + pupbis(dim)(e))), max(min(psum(d2(dim))(j)) + entry(dim, e, j)))).sum)
//            println(candidates(d2(dim)).filter(j => psum(d2(dim))(j) + pupe(d2(dim))(j) >= 0).map(j => max(max(entry(dim, e, j)) - max(min(entry(dim, e, j), psum(dim)(e) + pupbis(dim)(e))), max(min(psum(d2(dim))(j)) + entry(dim, e, j)))).mkString(", "))
//            println("--")
//            val tR = (ROWS + orient) % 2
//            println("R? : " + candidates(tR).mkString(", "))
//            println("r_i: " + candidates(tR).map(i => psum(tR)(i)).mkString(", "))
//            println("r_pup: " + candidates(tR).map(i => pup(tR)(i)).mkString(", "))
//            println("r_pupbis: " + candidates(tR).map(i => pupbis(tR)(i)).mkString(", "))
//            println("r_pupe: " + candidates(tR).map(i => pupe(tR)(i)).mkString(", "))
//            val tC = (COLS + orient) % 2
//            println("C? : " + candidates(tC).mkString(", "))
//            println("c_i: " + candidates(tC).map(i => psum(tC)(i)).mkString(", "))
//            println("c_pup: " + candidates(tC).map(i => pup(tC)(i)).mkString(", "))
//            println("c_pupbis: " + candidates(tC).map(i => pupbis(tC)(i)).mkString(", "))
//            println("c_pupe: " + candidates(tC).map(i => pupe(tC)(i)).mkString(", "))
//            sys.exit()
//          }
//        }
//      }
//    }
//  }
}
