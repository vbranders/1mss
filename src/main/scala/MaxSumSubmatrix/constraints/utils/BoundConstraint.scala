package MaxSumSubmatrix.constraints.utils

import oscar.algo.reversible.ReversibleDouble
import oscar.cp.Constraint
import oscar.cp.core.CPStore



abstract class BoundConstraint(store: CPStore) extends Constraint(store) {
  implicit def revDouble2Double(x: ReversibleDouble): Double = x.value

  def max(x: Double, y: Double = 0): Double = math.max(x, y)

  def min(x: Double, y: Double = 0): Double = math.min(x, y)

  def diff(x: Double, y: Double): Boolean = math.abs(x - y) > 1e-8

  def d2(d: Int): Int = (d + 1) % 2

}


// For reference only:
abstract class BConstraint(store: CPStore) extends Constraint(store) {

  def ub: Double

  def boundName: String

  def partialType: Int //0: normal sum of +, //1: on positive cjub, //2: expected...
  def is_valid(): Boolean

  def test_equal(x: Double, y: Double, what: String): Unit = {
    if (math.abs(x - y) > 1e-8) {
      println("Error validation " + boundName + " : " + what)
      println(x)
      println(y)
      sys.exit()
    }
  }

  def compute_bound(): Double

  implicit def revDouble2Double(x: ReversibleDouble): Double = x.value

  def max(x: Double): Double = math.max(0, x)

  def min(x: Double): Double = math.min(0, x)

  def d2(d: Int): Int = (d + 1) % 2

  def bindDimCell(dim: Int, e1: Int, e2: Int, psum: Double, pup: Double, plo: Double, oldPsum: Double, oldPup: Double, oldPlo: Double): Unit

  def bindDim(dim: Int, e: Int, psum: Array[Array[ReversibleDouble]], pup: Array[Array[ReversibleDouble]], plo: Array[Array[ReversibleDouble]]): Unit

  def filter(matrix: Array[Array[Double]], psum: Array[Array[ReversibleDouble]], pup: Array[Array[ReversibleDouble]], plo: Array[Array[ReversibleDouble]]): Unit
}


