package MaxSumSubmatrix.constraints.utils

import MaxSumSubmatrix.constraints.OneMSSConstraint
import MaxSumSubmatrix.constraints.reference.OneMSSBase
import MaxSumSubmatrix.runners.Utils.getCpuTime
import MaxSumSubmatrix.utils
import MaxSumSubmatrix.utils.MaximizeDouble
import oscar.algo.reversible.ReversibleDouble
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

/**
  * Created by vbranders on 03/09/2020.
  *
  */

trait ConstraintMethod {
  def name: String

  def make(cp: CPSolver, matrix: Array[Array[Double]], rows: Array[CPBoolVar], cols: Array[CPBoolVar], obj: utils.DoubleVar, ub: Double, ctrObj: MaximizeDouble, args: Array[String]): CurrentBoundWithHeuristicTraits //Optionally returns something with trait HeuristicTrait which can be used for branching
}

trait CurrentBoundWithHeuristicTraits extends CurrentBoundTrait with HeuristicTrait {
  protected implicit def revDouble2Double(x: ReversibleDouble): Double = x.value

  var start = 0.0
  var bestSol = 0.0

  def d2(d: Int): Int = (d + 1) % 2

  def onSolution(rows: Array[Int], cols: Array[Int]): Unit

  def started(): Unit = {
    start = getCpuTime
  }

  def getBestSolution: Double = bestSol

  def checkNew(): Unit
}

trait CurrentBoundTrait {
  /**
    * @return the bound by selecting only already-1 rows and the optimal selection of cols
    */
  def curBoundRows0: Double

  def curBoundRows0Apply(): Unit

  /**
    * @return the bound by selecting only already-1 cols and the optimal selection of rows
    */
  def curBoundCols0: Double

  def curBoundCols0Apply(): Unit
}

trait HeuristicTrait {
  def colHeuristic(j: Int): Double
}


// Base method
object BrandersEtAl extends ConstraintMethod {
  override def name: String = "Base"

  override def make(cp: CPSolver, matrix: Array[Array[Double]], rows: Array[CPBoolVar], cols: Array[CPBoolVar], obj: utils.DoubleVar, ub: Double, ctrObj: MaximizeDouble, args: Array[String]): CurrentBoundWithHeuristicTraits = {
    val currentBound = new OneMSSBase(rows, cols, matrix, obj, ub, ctrObj, pruneUp = args.contains("pruneUp"), pruneLow = args.contains("pruneLow"), doFilter = args.contains("doFilter"))
    cp.post(currentBound)
    currentBound
  }
}

object BrandersNew extends ConstraintMethod {
  override def name: String = "New"

  override def make(cp: CPSolver, matrix: Array[Array[Double]], rows: Array[CPBoolVar], cols: Array[CPBoolVar], obj: utils.DoubleVar, ub: Double, ctrObj: MaximizeDouble, args: Array[String]): CurrentBoundWithHeuristicTraits = {
    val currentBound = new OneMSSConstraint(rows, cols, matrix, obj, ub, ctrObj, args)
    cp.post(currentBound)
    currentBound
  }
}
