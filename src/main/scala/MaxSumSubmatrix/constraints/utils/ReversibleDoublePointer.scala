package MaxSumSubmatrix.constraints.utils

import oscar.algo.reversible.{Reversible, ReversibleContext, TrailEntry}

/**
  * Created by vbranders on 24/09/2020.
  *
  */
class ReversibleDouble2TrailEntry[T](reversible: ReversibleDoublePointer[T], value: T, secondValue: T) extends TrailEntry {
  override def restore(): Unit = {
    reversible.restore(value, secondValue)
  }
}

class ReversibleDoublePointer[@specialized T](final override val context: ReversibleContext, initialValue: T) extends Reversible {

  // Reference on the current value
  protected var pointer: T = initialValue
  protected var secondPointer: T = initialValue

  @inline override def trailEntry: TrailEntry = {
    new ReversibleDouble2TrailEntry[T](this, pointer, secondPointer)
  }

  @inline final def setValue(value: T): Unit = {
    if (value != pointer) {
      trail()
      this.secondPointer = pointer
      this.pointer = value
    }
  }

  /** @param value to assign */
  @inline final def value_= (value: T): Unit = setValue(value)

  /** @param value to assign */
  final def := (value: T): Unit = setValue(value)

  /** @return current value */
  @inline final def value: T = pointer

  /** @return old value */
  @inline final def prev: T = secondPointer

  /**
    * Check if the pointer is different from null
    * @return true if the pointer is != null, false otherwise
    */
  @inline final def hasValue[T]: Boolean = pointer != null

  /** @return the current pointer */
  @inline final def getValue(): T = pointer

  @inline final def restore(value: T, secondValue: T): Unit = {
    pointer = value
    secondPointer = secondValue
  }

  override def toString(): String = if (hasValue) pointer.toString else ""
}

object ReversibleDoublePointer {
  def apply[T](node: ReversibleContext, value: T): ReversibleDoublePointer[T] = {
    new ReversibleDoublePointer[T](node, value)
  }
  implicit def reversiblePointerToValue[@specialized T](reversible: ReversibleDoublePointer[T]): T = reversible.value
}
