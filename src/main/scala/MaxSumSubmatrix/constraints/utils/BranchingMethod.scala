package MaxSumSubmatrix.constraints.utils

import MaxSumSubmatrix.branchings.ImmediateSolution
import MaxSumSubmatrix.utils
import MaxSumSubmatrix.utils.MaximizeDouble
import oscar.algo.branchings.BinaryStaticOrderBranching
import oscar.algo.search.Branching
import oscar.cp.core.CPSolver
import oscar.cp.core.variables.CPBoolVar

/**
  * Created by vbranders on 03/09/2020.
  *
  */

trait BranchingMethod {
  def name: String

  def make(cp: CPSolver, matrix: Array[Array[Double]], rows: Array[CPBoolVar], cols: Array[CPBoolVar], obj: utils.DoubleVar, ub: Double, ctrObj: MaximizeDouble, bound: CurrentBoundWithHeuristicTraits): Branching
}

// Static method
object StaticBranchingMethod extends BranchingMethod {
  override def name: String = "Static"

  override def make(cp: CPSolver, matrix: Array[Array[Double]], rows: Array[CPBoolVar], cols: Array[CPBoolVar], obj: utils.DoubleVar, ub: Double, ctrObj: MaximizeDouble, bound: CurrentBoundWithHeuristicTraits): Branching = {
    val variables = sort(1, cols, rows, matrix) ++ sort(0, rows, cols, matrix)
    new ImmediateSolution(new BinaryStaticOrderBranching(variables.toArray, e => variables(e).max), rows, cols, ctrObj, bound)
  }

  def sort(orient: Int, vars: Array[CPBoolVar], d2vars: Array[CPBoolVar], matrix: Array[Array[Double]]): Array[CPBoolVar] = {
    vars.indices.sortBy(e => -d2vars.indices.map(d2e => if (orient == 0) {
      matrix(e)(d2e)
    } else matrix(d2e)(e)).filter(_ > 0).sum).map(vars(_))
  }
}
