package MaxSumSubmatrix.constraints.utils

import oscar.algo.reversible.{ReversibleContext, TrailEntry}

/**
  * Created by vbranders on 24/09/2020.
  *
  */
class ReversibleDoubleDoubleTrailEntry(reversible: ReversibleDoubleDouble, value: Double, secondValue: Double) extends TrailEntry {
  @inline override final def restore(): Unit = {
    reversible.restore(value, secondValue)
  }
}

class ReversibleDoubleDouble(node: ReversibleContext, value: Double) extends ReversibleDoublePointer[Double](node, value) {

  @inline final override def trailEntry = new ReversibleDoubleDoubleTrailEntry(this, pointer, secondPointer)

  /** Increments the reversible integer by i */
  def +=(v: Double): Double = {
    trail()
    pointer += v
    secondPointer = pointer - v
    pointer
  }

  /** Decrements the reversible integer by i */
  def -=(v: Double): Double = {
    trail()
    pointer -= v
    secondPointer = pointer + v
    pointer
  }

  def delta(): Double = {
    secondPointer.doubleValue() - pointer.doubleValue()
  }
}

object ReversibleDoubleDouble {
  def apply(value: Double)(implicit context: ReversibleContext) = new ReversibleDoubleDouble(context, value)
}
