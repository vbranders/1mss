package MaxSumSubmatrix.constraints

import MaxSumSubmatrix.constraints.alternatives.{Common1, Common2, Common3}
import MaxSumSubmatrix.constraints.utils.{CurrentBoundWithHeuristicTraits, ReversibleDoubleDouble}
import MaxSumSubmatrix.runners.Utils.getCpuTime
import MaxSumSubmatrix.utils.{DoubleVar, MaximizeDouble}
import oscar.algo.Inconsistency
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.CPVar
import oscar.cp.{CPBoolVar, Constraint}


/**
  * Created by vbranders on 03/09/2020.
  *
  *
  * Bind cols 3, true
  * 18.3/0.0 / 47.8+47.8 / 0.0/47.8
  * Bind row 1
  * Bind col 0
  * Propagate on 1
  * Propagate on 0
  * Propagate --------
  * Propagate on 1
  * Propagate on 0
  * Propagate --------
  * Branching
  */
//TODO: Should use different propagate when changing bound (obj.min) or binding a variable to prevent repeating propagate twice
class OneMSSConstraint(rows: Array[CPBoolVar],
                       cols: Array[CPBoolVar],
                       matrix: Array[Array[Double]],
                       obj: DoubleVar,
                       ub: Double,
                       ctrObj: MaximizeDouble,
                       args: Array[String],
                       epsilon: Double = 1e-8) extends Constraint(rows(0).store) with CurrentBoundWithHeuristicTraits {

  private[this] val cp = rows(0).store
  private[this] val psum: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
  //The three different types
  private[this] val pup: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
//  private[this] val pupbis: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
//  private[this] val pupe: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
  private[this] val plo: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))

  private[this] val psum_prev: Array[Array[Double]] = psum.map(e => e.map(_ => 0.0))
  private[this] val pup_prev: Array[Array[Double]] = pup.map(e => e.map(_ => 0.0))
//  private[this] val pupbis_prev: Array[Array[Double]] = pup.map(e => e.map(_ => 0.0))
//  private[this] val pupe_prev: Array[Array[Double]] = pup.map(e => e.map(_ => 0.0))
//  private[this] val plo_prev: Array[Array[Double]] = plo.map(e => e.map(_ => 0.0))

  private[this] val curBound: Array[ReversibleDouble] = Array.tabulate(2)(_ => new ReversibleDouble(cp, 0.0))
  private[this] val lastUnbound: Array[ReversibleSparseSet] = Array.tabulate(2)(dim => new ReversibleSparseSet(cp, 0, Array(rows.length, cols.length)(dim) - 1))

  private[this] val argBoundsApplied: Array[Boolean] = Array.tabulate(10)(i => args.contains((i + 1).toString).toString).map(a => a.toBoolean)
  private[this] val argBoundSum: Array[Int] = argBoundsApplied.map(e => if(e){1} else 0).scanLeft(0)(_ + _)//Array.tabulate(argBoundsApplied.size)(i => (argBoundsApplied.red.foldLeft(0)((a,b) => a + {if(b){1} else 0}))

  private[this] val upBound: Array[Array[ReversibleDouble]] = argBoundsApplied.indices.filter(i => argBoundsApplied(i)).map(_ => Array.tabulate(2)(_ => new ReversibleDouble(cp, ub))).toArray

  private[this] val bounds: Array[Constraint] = argBoundsApplied.indices.filter(i => argBoundsApplied(i)).map(i => getBnd(i)).toArray
//  private[this] val check_validity: Boolean = false

  val pruneUp: Boolean = args.contains("pruneUp")
  val pruneLow: Boolean = args.contains("pruneLow")

  var preparing = false

  override def associatedVars(): Iterable[CPVar] = rows ++ cols

  override def setup(l: CPPropagStrength): Unit = {
    for (i <- rows.indices) {
      for (j <- cols.indices) {
        if (m(i, j) > 0) {
          pup(0)(i) += m(i, j)
          pup(1)(j) += m(i, j)
        } else {
          plo(0)(i) += m(i, j)
          plo(1)(j) += m(i, j)
        }
      }
    }

    bounds.foreach(bound => cp.post(bound))//.setup())

    for (i <- rows.indices) {
      rows(i).filterWhenBind() {
        prepBranch()
        bind(0, i)
        false
      }
      rows(i).callPropagateWhenBind(this)
    }
    for (j <- cols.indices) {
      cols(j).filterWhenBind() {
        prepBranch()
        bind(1, j)
        false
      }
      cols(j).callPropagateWhenBind(this)
    }

    obj.callPropagateWhenBoundsChange(this)
    priorityL2 = oscar.cp.core.CPStore.MinPriorityL2 //Should be 0 and others +1
    propagate()
  }

  def prepBranch(): Unit = {
//    if (preparing) {
//      println("Branched!")
//    }
    preparing = false
  }

  def bind(dim: Int, e: Int): Unit = {
    lastUnbound(dim).removeValue(e)
//    println("Bind " + which(dim) + " " + e + ", " + variable(dim, e).isTrue)
    val otherDim: Int = d2(dim)

    for(dim <- Array(0, 1)){
      System.arraycopy(psum(dim).map(e => e.value), 0, psum_prev(dim), 0, psum(dim).length)
      System.arraycopy(pup(dim).map(e => e.value), 0, pup_prev(dim), 0, pup(dim).length)
    }
    for (e2 <- possible(otherDim)) {
      val x = entry(dim, e, e2)
      if (variable(dim, e).isTrue) {
        psum(otherDim)(e2) += x
      }
      if (x > 0) {
        pup(otherDim)(e2) -= x
      } else {
        plo(otherDim)(e2) -= x
      }

      val delta = getDelta(psum_prev(otherDim)(e2), psum(otherDim)(e2).value, !lastUnbound(otherDim).hasValue(e2))
      if (delta != 0) {
        curBound(otherDim) += delta
      }

      if (!variable(otherDim, e2).isBound) {
        if (pruneUp && psum(otherDim)(e2).value + pup(otherDim)(e2).value < - epsilon) {
          variable(otherDim, e2).assign(0)
        }
        //not done in original paper !
//        if (pruneLow && psum(otherDim)(e2).value + plo(otherDim)(e2).value > epsilon) {
//          variable(otherDim, e2).assign(1)
//        }
      }
    }
    if (variable(dim, e).isTrue) {
      if (psum(dim)(e) < 0) {
        curBound(dim) += psum(dim)(e).value
      }
    } else {
      if (psum(dim)(e) > 0) {
        curBound(dim) -= psum(dim)(e).value
      }
    }
    //TODO : do here also
//    bounds.map(pairs => pairs.indices.map(dim2 => pairs(dim2).bindDim(dim, e)))

//    println(curBound.mkString("/") + " / " + upBound(0).map(x => x.value).mkString("+") + " / " + obj.min + "/" + obj.max)
  }

  def entry(dim: Int, e: Int, d2e: Int): Double = if (dim == 0) {
    m(e, d2e)
  } else m(d2e, e)

  def m(i: Int, j: Int): Double = matrix(i)(j)

  def possible(dim: Int): Array[Int] = {
    {for(e <- if(dim==0){rows.indices}else cols.indices if lastUnbound(dim).hasValue(e) || variable(dim, e).isTrue) yield e}.toArray
  }

  def candidates(dim: Int): Array[Int] = {
    lastUnbound(dim).toArray //TODO: do not leave .toArray and when lastUnbound is modified, create the array copy
    //    for(e <- (if(dim==0){rows}else cols).indices if lastUnbound(dim).hasValue(e)) yield e
  }

  def selected(dim: Int): Array[Int] = {
    for(e <- lastUnbound(dim).removedToArray if variable(dim, e).isTrue) yield e
//    for (e <- lastUnbound(dim).removedToArray if variable(dim, e).isTrue) yield e
//    for(e <- (if(dim==0){rows}else cols).indices if variable(dim, e).isTrue && !lastUnbound(dim).hasValue(e)) yield e
  }

  def variable(dim: Int, element: Int): CPBoolVar = {
    if (dim == 0) {
      rows(element)
    } else {
      cols(element)
    }
  }


  @inline
  def which(dim: Int): String = if (dim == 0) {
    "rows"
  } else "cols"


//  val COLS = 1
//  val ROWS = 0
//  private[this] val tmp_var: Array[Array[ReversibleInt]] =  Array.tabulate(2)(dimension => Array.fill({if(dimension==ROWS) rows.length else cols.length})(new ReversibleInt(cp, -1)))
//  def get_temp_sol(): Int = {
//    tmp_var(0).count(i => i.value != -1) + tmp_var(1).count(j => j.value != -1)
//  }
//  def apply_temp_sol(): Unit = {
//    val sCols = for(e <- cols.indices if (variable(COLS, e).isTrue && !lastUnbound(COLS).hasValue(e) && tmp_var(COLS)(e).value == -1) || tmp_var(COLS)(e).value == 1) yield e
//    val cCols = for(e <- cols.indices if lastUnbound(COLS).hasValue(e) && tmp_var(COLS)(e).value == -1) yield e
//    val sRows = for(e <- rows.indices if (variable(ROWS, e).isTrue && !lastUnbound(ROWS).hasValue(e) && tmp_var(ROWS)(e).value == -1) || tmp_var(ROWS)(e).value == 1) yield e
//    val cRows = for(e <- rows.indices if lastUnbound(ROWS).hasValue(e) && tmp_var(ROWS)(e).value == -1) yield e
//    val curSel = sRows.map(i => sCols.map(j => matrix(i)(j)).sum).sum
//    val curBndCol0 = curSel + cRows.map(i => math.max(0, sCols.map(j => matrix(i)(j)).sum)).sum
//    val curBndRow0 = curSel + cCols.map(j => math.max(0, sRows.map(i => matrix(i)(j)).sum)).sum
//
//    if(math.max(curBndCol0, curBndRow0) > bestSol) {
//      println("New good intermediate !! " + math.max(curBndCol0, curBndRow0) + " / " + obj.max)
//      bestSol = math.max(curBndCol0, curBndRow0)
//      if(curBndCol0 > curBndRow0){
//        onSolution(sCols.toArray, sCols.toArray)
//      }
//      ctrObj.updateLB(math.max(curBndCol0, curBndRow0))
//      cp.propagate()
//    }
//    throw Inconsistency.get
//  }

  override def propagate(): Unit = {
//    println("Propagate --------")
//    if(true) {
//      for (dim <- Array(ROWS, COLS)) {
//        val otherDim = (dim + 1) % 2
//        for (e <- (if (dim == ROWS) {
//          rows
//        } else {
//          cols
//        }).indices if variable(dim, e).isFalse && !lastUnbound(dim).hasValue(e)) {
//          val lowest = selected(otherDim).map(d2e => entry(dim, e, d2e)).sum + candidates(otherDim).map(d2e => math.min(0, entry(dim, e, d2e))).sum
//          if (lowest > epsilon) {
//            tmp_var(dim)(e) := 1
//          }
//        }
//        for (e <- selected(dim)) {
//          if (psum(dim)(e).value + pup(dim)(e).value < 0) {
//            tmp_var(dim)(e) := 0
//          }
//        }
//      }
//      for (e <- selected(0)) {
//        for (d2e <- selected(1)) {
//          if (psum(0)(e) + psum(1)(d2e) + pup(0)(e) + pup(1)(d2e) - entry(0, e, d2e) < -epsilon) {
//            tmp_var(0)(e) := 0
//            tmp_var(1)(d2e) := 0
//          }
//        }
//      }
//    }
//    if(get_temp_sol() > 0){
//      apply_temp_sol()
//    }
    // Must be called last!
    obj.updateMin(cb)
    if (cb < 0) {
      println("It Happened")
      throw Inconsistency.get
    }
  }

  /**
    * @return the current bound for the pure problem
    */
  def cb: Double = Math.max(curBound(0).value, curBound(1).value)

//  def filter(): Unit = {
//    bounds.map(pairs => pairs.indices.map(dim2 => pairs(dim2).filter()))
//  }

  /**
    * @return the bound by selecting only already-1 rows and the optimal selection of cols
    */
  override def curBoundRows0: Double = curBound(1).value

  override def curBoundRows0Apply(): Unit = zero(0)

  def getZeros(vars: Array[CPBoolVar]): Array[Int] = {
    vars.map(e => if(e.isTrue){1} else 0)
  }
  def getBests(vars: Array[CPBoolVar], curSum: Array[ReversibleDoubleDouble]): Array[Int] = {
    vars.indices.map(e => if(vars(e).isTrue || (!vars(e).isBound && curSum(e).value > 0)){1} else 0).toArray
  }

  @inline
  def zero(dim: Int): Unit = {
//    println("Set all candidates " + which(dim) + " to zero")
//    if(dim == 0){
//      onSolution(getZeros(rows), getBests(cols, psum(1)))
//    } else {
//      onSolution(getBests(rows, psum(0)), getZeros(cols))
//    }
    try {
      setToZero(if (dim == 0) {
        rows
      } else cols)
      setToBest(if (dim == 0) {
        cols
      } else rows, psum((dim + 1) % 2))
      cols(0).store.propagate()
//      println("(all fixed)")
    } catch {
      case _: Throwable =>
      //throw new RuntimeException("Should never happen")
    }
  }

  @inline
  def setToZero(vars: Array[CPBoolVar]): Unit = {
    for (x <- vars)
      if (!x.isBound)
        x.assign(0)
  }

  @inline
  def setToBest(vars: Array[CPBoolVar], bound: Array[ReversibleDouble]): Unit = {
    for (i <- vars.indices)
      if (!vars(i).isBound)
        vars(i).assign(if (bound(i).value > 0) 1 else 0)
  }

  /**
    * @return the bound by selecting only already-1 cols and the optimal selection of rows
    */
  override def curBoundCols0: Double = curBound(0).value

  override def curBoundCols0Apply(): Unit = zero(1)

  override def colHeuristic(j: Int): Double = pup(1)(j).value

  override def onSolution(rows: Array[Int], cols: Array[Int]): Unit = {
    bestSol = math.max(bestSol, obj.min)
    println("Best: " + bestSol + "/" + ub + " > " + obj.min + "/" + obj.max + " \t\t" + ((getCpuTime - start) / 1000000.0))
//    println("Recompute: " + recompute(matrix, rows, cols))
    println("\t" + rows.indices.count(e => rows(e) == 1) + "/" + rows.length + "\t" + cols.indices.count(e => cols(e) == 1) + "/" + cols.length)
//    println(rows.indices.filter(e => rows(e) == 1).mkString(","))
//    println(cols.indices.filter(e => cols(e) == 1).mkString(","))
    checkNew()
    //TODO
  }

  override def checkNew(): Unit = {
//    println("Preparing a branching !!")
    preparing = true
  }

  @inline
  private def getDelta(initialValue: Double, newValue: Double, requiredDueToSelection: Boolean): Double = {
    if (requiredDueToSelection || initialValue > 0 && newValue > 0) {
      newValue - initialValue
    } else if (initialValue > 0) {
      -initialValue
    } else if (newValue > 0) {
      newValue
    } else {
      0.0
    }
  }

  def recompute(matrix: Array[Array[Double]], rows: Array[Int], cols: Array[Int]): Double = {
    var sum = 0.0
    for (i <- rows.indices.filter(e => rows(e) == 1)) {
      for (j <- cols.indices.filter(e => cols(e) == 1)) {
        sum += matrix(i)(j)
      }
    }
    sum
  }

  def getBnd(i: Int): Constraint = {
    i + 1 match {
      case 1 => new Common1(rows, cols, matrix, obj, upBound(argBoundSum(i)), psum, pup, psum_prev, pup_prev, lastUnbound, args.contains("doFilter"), epsilon)
      case 2 => new Common2(rows, cols, matrix, obj, upBound(argBoundSum(i)), psum, pup, plo, psum_prev, pup_prev, lastUnbound, args.contains("doFilter"), epsilon)
      case 3 => new Common3(rows, cols, matrix, obj, upBound(argBoundSum(i)), psum, pup, lastUnbound, args.contains("doFilter"), epsilon)
    }
  }
  def max(x: Double, y: Double = 0): Double = math.max(x, y)
  def min(x: Double, y: Double = 0): Double = math.min(x, y)
}
