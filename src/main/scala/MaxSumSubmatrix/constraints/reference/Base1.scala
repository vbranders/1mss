package MaxSumSubmatrix.constraints.reference

import MaxSumSubmatrix.utils.DoubleVar
import oscar.algo.Inconsistency
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.Constraint
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Created by vbranders on 16/09/2020.
  *
  */
class Base1(
             rows: Array[CPBoolVar], cols: Array[CPBoolVar],
             matrix: Array[Array[Double]],
             obj: DoubleVar,
             psum: Array[Array[ReversibleDouble]],
             pup: Array[Array[ReversibleDouble]],
             plo: Array[Array[ReversibleDouble]],
             upBound: Array[ReversibleDouble],
             curBound: Array[ReversibleDouble],
             lastUnbound: Array[ReversibleSparseSet],
             pruneUp: Boolean,
             pruneLow: Boolean,
             doFilter: Boolean,
             orient: Int) extends Constraint(rows(0).store) {

  val ubIn: Array[Double] = Array.fill(rows.length)(0.0)
  val ubOut: Array[Double] = Array.fill(rows.length)(0.0)
  val usableRows: Array[Int] = Array.fill(rows.length)(0)

  override def setup(l: CPPropagStrength): Unit = {
    for (i <- rows.indices) {
      if (rows(i).isBound) {
        bindRow(i)
      }
      rows(i).filterWhenBind() {
        bindRow(i)
        false
      }
      if (doFilter) {
        rows(i).callPropagateWhenBind(this)
      }
    }
    if (doFilter) {
      for (j <- cols) {
        j.callPropagateWhenBind(this)
      }
    }
    obj.callPropagateWhenBoundsChange(this)
    priorityL2 = oscar.cp.core.CPStore.MinPriorityL2 + 1
  }

  private def bindRow(i: Int): Unit = {
//    println("Bind " + which(orient) + " " + i + " to " + rows(i).isTrue)
    lastUnbound(0).removeValue(i)

    // update c/cUpPlus/curBoundC/uppBoundC
    var j = 0
    while (j != cols.length) {
      bindRowCell(i, j)
      j += 1
    }

    // update curBoundR/uppBoundR
    if (rows(i).isTrue) {
      if (psum(0)(i) < 0) {
        curBound(0) += psum(0)(i)
      }
      if (psum(0)(i) + pup(0)(i) < 0) {
        upBound(0) += psum(0)(i) + pup(0)(i)
        obj.updateMax(upBound(0).value)
      }
    }
    else {
      if (psum(0)(i) > 0) {
        curBound(0) -= psum(0)(i)
      }
      if (psum(0)(i) + pup(0)(i) > 0) {
        upBound(0) -= psum(0)(i) + pup(0)(i)
        obj.updateMax(upBound(0).value)
      }
    }
  }

  @inline
  private def bindRowCell(i: Int, j: Int): Unit = {
    val colIsBot = lastUnbound(1).hasValue(j)
    val colIsSelected = !colIsBot && cols(j).isTrue
    if (colIsBot || colIsSelected) {
      val rowIsSelected = rows(i).isTrue
      val oldC: Double = psum(1)(j)
      val oldCUp: Double = pup(1)(j)

      if (rowIsSelected)
        psum(1)(j) += matrix(i)(j)
      if (matrix(i)(j) > 0)
        pup(1)(j) -= matrix(i)(j)
      else
        plo(1)(j) -= matrix(i)(j)

      val deltaCur = getDelta(oldC, psum(1)(j), colIsSelected)
      val deltaUpp = getDelta(oldC + oldCUp, psum(1)(j) + pup(1)(j), colIsSelected)

      if (deltaCur != 0)
        curBound(1) += deltaCur

      if (deltaUpp != 0) {
        upBound(1) += deltaUpp
        obj.updateMax(upBound(1).value)
      }

      if (!cols(j).isBound) {
        if (pruneUp && psum(1)(j) + pup(1)(j) < -1e-8)
          cols(j).assign(0)
        if (pruneLow && psum(1)(j) + plo(1)(j) > 1e-8)
          cols(j).assign(1)
      }
    }
  }

  @inline
  private def getDelta(origval: Double, newval: Double, presenceForced: Boolean): Double = {
    if (presenceForced || origval > 0 && newval > 0) {
      newval - origval
    } else if (origval > 0) {
      -origval
    } else if (newval > 0) {
      newval
    } else {
      0.0
    }
  }

  @inline
  def which(dim: Int): String = if (dim == 0) {
    "rows"
  } else "cols"

  override def propagate(): Unit = filter()

  private def filter(): Unit = {
    val nbRows = lastUnbound(0).fillArray(usableRows)

    var t = 0
    while (t != nbRows) {
      ubIn(t) = 0.0
      ubOut(t) = 0.0
      t += 1
    }

    var j = 0
    while (j != cols.length) {
      if (!cols(j).isFalse) {
        val colIsTrue = cols(j).isTrue

        t = 0
        while (t != nbRows) {
          val i = usableRows(t)

          val cUp = psum(1)(j).value + pup(1)(j).value

          if (matrix(i)(j) > 0) {
            if (cUp - matrix(i)(j) > 0 || colIsTrue)
              ubOut(t) += cUp - matrix(i)(j)
            ubIn(t) += math.max(0, cUp)
          }
          else {
            ubOut(t) += math.max(cUp, 0)
            if (cUp + matrix(i)(j) > 0 || colIsTrue)
              ubIn(t) += cUp + matrix(i)(j)
          }
          t += 1
        }
      }
      j += 1
    }

    t = 0
    while (t != nbRows) {
      val i = usableRows(t)
      if (ubIn(t) <= obj.min - 1e-8 && ubOut(t) <= obj.min - 1e-8) {
//        println("Inconsistency")
        throw Inconsistency.get
      }

      if (ubIn(t) <= obj.min - 1e-8) {
//        println("Need to remove " + i)
        rows(i).assignFalse()
      }
      if (ubOut(t) <= obj.min - 1e-8) {
//        println("Need to take " + i)
        rows(i).assignTrue()
      }
      t += 1
    }
  }

  override def associatedVars(): Iterable[CPVar] = rows ++ cols

  implicit def revDouble2Double(x: ReversibleDouble): Double = x.value
}
