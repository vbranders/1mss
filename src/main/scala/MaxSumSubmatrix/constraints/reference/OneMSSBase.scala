package MaxSumSubmatrix.constraints.reference

import MaxSumSubmatrix.constraints.utils.CurrentBoundWithHeuristicTraits
import MaxSumSubmatrix.runners.Utils.getCpuTime
import MaxSumSubmatrix.utils.{DoubleVar, MaximizeDouble}
import oscar.algo.Inconsistency
import oscar.algo.reversible.{ReversibleDouble, ReversibleSparseSet}
import oscar.cp.Constraint
import oscar.cp.core.CPPropagStrength
import oscar.cp.core.variables.{CPBoolVar, CPVar}

/**
  * Created by vbranders on 03/09/2020.
  *
  */
class OneMSSBase(rows: Array[CPBoolVar],
                 cols: Array[CPBoolVar],
                 matrix: Array[Array[Double]],
                 obj: DoubleVar,
                 ub: Double,
                 ctrObj: MaximizeDouble,
                 pruneUp: Boolean,
                 pruneLow: Boolean,
                 doFilter: Boolean) extends Constraint(rows(0).store) with CurrentBoundWithHeuristicTraits {

  private[this] val cp = rows(0).store
  val curBound: Array[ReversibleDouble] = Array.fill(2)(new ReversibleDouble(cp, 0.0))
  val upBound: Array[ReversibleDouble] = Array.fill(2)(new ReversibleDouble(cp, 0.0))
  private[this] val psum: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
  private[this] val pup: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
  private[this] val plo: Array[Array[ReversibleDouble]] = Array.tabulate(2)(dim => Array.fill(Array(rows.length, cols.length)(dim))(new ReversibleDouble(cp, 0)))
  private[this] val lastUnbound: Array[ReversibleSparseSet] = Array.tabulate(2)(dim => new ReversibleSparseSet(cp, 0, Array(rows.length, cols.length)(dim) - 1))
  var preparing = false

  override def associatedVars(): Iterable[CPVar] = rows ++ cols

  //  def up(dim: Int, e: Int): Double = psum(dim)(e) + pup(dim)(e)
  //  def lo(dim: Int, e: Int): Double = psum(dim)(e) + plo(dim)(e)
  def entry(dim: Int, e: Int, d2e: Int): Double = if (dim == 0) {
    m(e, d2e)
  } else m(d2e, e)

  def m(i: Int, j: Int): Double = matrix(i)(j)

  override def setup(l: CPPropagStrength): Unit = {
    for (i <- rows.indices) {
      for (j <- cols.indices) {
        if (m(i, j) > 0) {
          pup(0)(i) += m(i, j)
          pup(1)(j) += m(i, j)
          upBound(0) += m(i, j)
          upBound(1) += m(i, j)
        } else {
          plo(0)(i) += m(i, j)
          plo(1)(j) += m(i, j)
        }
      }
    }

    cp.post(new Base1(rows, cols, matrix, obj, psum, pup, plo, upBound, curBound, lastUnbound, pruneUp, pruneLow, doFilter, 0))
    cp.post(new Base1(cols, rows, matrix.transpose, obj, psum.reverse, pup.reverse, plo.reverse, upBound.reverse, curBound.reverse, lastUnbound.reverse, pruneUp, pruneLow, doFilter, 1))

    for (i <- rows) {
      i.filterWhenBind() {
        if (preparing) {
//          println("Branched!")
        }
        preparing = false
        false
      }
      i.callPropagateWhenBind(this)
    }
    for (j <- cols) {
      j.filterWhenBind() {
        if (preparing) {
//          println("Branched!")
        }
        preparing = false
        false
      }
      j.callPropagateWhenBind(this)
    }
    obj.callPropagateWhenBoundsChange(this)
    priorityL2 = oscar.cp.core.CPStore.MinPriorityL2
    propagate()
  }

  override def propagate(): Unit = {
    // Must be called last!
    obj.updateMin(cb)
    if (cb < 0) {
      println("It Happened")
      throw Inconsistency.get
    }
  }

  /**
    * @return the current bound for the pure problem
    */
  def cb: Double = Math.max(curBound(0).value, curBound(1).value)

  /**
    * @return the bound by selecting only already-1 rows and the optimal selection of cols
    */
  override def curBoundRows0: Double = curBound(1).value

  override def curBoundRows0Apply(): Unit = zero(0)

  /**
    * @return the bound by selecting only already-1 cols and the optimal selection of rows
    */
  override def curBoundCols0: Double = curBound(0).value

  override def curBoundCols0Apply(): Unit = zero(1)

  @inline
  def zero(dim: Int): Unit = {
//    println("Set all candidates " + which(dim) + " to zero")
    try {
      setToZero(if (dim == 0) {
        rows
      } else cols)
      setToBest(if (dim == 0) {
        cols
      } else rows, psum((dim + 1) % 2))
      cols(0).store.propagate()
    } catch {
      case _: Throwable =>
      //throw new RuntimeException("Should never happen")
    }
  }

  @inline
  def setToZero(vars: Array[CPBoolVar]): Unit = {
    for (x <- vars)
      if (!x.isBound)
        x.assign(0)
  }

  @inline
  def setToBest(vars: Array[CPBoolVar], bound: Array[ReversibleDouble]): Unit = {
    for (i <- vars.indices)
      if (!vars(i).isBound)
        vars(i).assign(if (bound(i).value > 0) 1 else 0)
  }

  @inline
  def which(dim: Int): String = if (dim == 0) {
    "rows"
  } else "cols"

  override def colHeuristic(j: Int): Double = pup(1)(j).value

  override def onSolution(rows: Array[Int], cols: Array[Int]): Unit = {
    bestSol = math.max(bestSol, obj.min)
    println("Best: " + bestSol + "/" + ub + " > " + obj.min + "/" + obj.max + " \t\t" + ((getCpuTime - start) / 1000000.0))
    println("Recompute: " + recompute(matrix, rows, cols))
    println(rows.indices.filter(e => rows(e) == 1).mkString(","))
    println(cols.indices.filter(e => cols(e) == 1).mkString(","))
    checkNew()
    //TODO
  }

  override def checkNew(): Unit = {
//    println("Preparing a branching !!")
    preparing = true
  }

  def recompute(matrix: Array[Array[Double]], rows: Array[Int], cols: Array[Int]): Double = {
    var sum = 0.0
    for (i <- rows.indices.filter(e => rows(e) == 1)) {
      for (j <- cols.indices.filter(e => cols(e) == 1)) {
        sum += matrix(i)(j)
      }
    }
    sum
  }
}
